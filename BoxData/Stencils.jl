module Stencils

export Stencil, applyStencil, applyStencilForEach!, composeStencils!, shift!, pruneStencil!, cleanupStencil!

"""A type to support stencil base operations. A stencil is defined by the dot product of a number of values and corresponding weights. This supports cell values coming from multiple different sources. """
struct Stencil
    cells::Vector{CartesianIndex}
    blocks::Vector{Int}
    weights::Vector
    # Stencil(cells, blocks) = new(cells, blocks,
    #                              zeros(size(cells)))
    Stencil(size::Int) = new(Vector{CartesianIndex}(undef, size),
                             ones(size),
                             zeros(size))
end

import Base
function Base.display(sten::Stencil)
    println(" Cells:  ", [isassigned(sten.cells, i) ? sten.cells[i].I : "#undef" for i in eachindex(sten.cells)])
    println(" Blocks: ", sten.blocks)
    println(" Weights:", sten.weights)
end

function Base.length(sten::Stencil)
    @assert length(sten.cells) == length(sten.blocks) == length(sten.weights)
	return length(sten.cells)
end

function Base.:+(x::Stencil, y::Stencil)::Stencil
    xl = length(x)
    yl = length(y)
    r = Stencil(xl+yl)
    r.cells[1:xl] = x.cells
    r.blocks[1:xl] = x.blocks
    r.weights[1:xl] = x.weights
    r.cells[(1:yl).+xl] = y.cells
    r.blocks[(1:yl).+xl] = y.blocks
    r.weights[(1:yl).+xl] = y.weights
    cleanupStencil!(r)
    return r
end

# function Base.resize!(sten::Stencil, sz::Int)
#     resize!(sten.cells, sz)
#     resize!(sten.blocks, sz)
#     resize!(sten.weights, sz)
# end

"""Evaluate the stencil dot product given a set of data.
The data must be index by first a region, and then the cell (think array of arrays) """
function applyStencil(sten::Stencil, data, center::CartesianIndex)
    #dot(weights, ld[sten.cells])
    stenVal = 0
    @assert length(sten.cells) == length(sten.blocks) == length(sten.weights)
    for (c, b, w) in zip(sten.cells, sten.blocks, sten.weights)
        stenVal += w * data[b][c + center]
    end
    return stenVal
end

function applyStencilForEach!(opval, sten::Stencil, data, region)
	for i in region
        opval[i] = applyStencil(sten, data, i)
    end
end

"""Inject the inner stencil to a element of the outer stencil"""
function composeStencils!(outer::Stencil, inner::Stencil, index::Int)
    # add the new stencil elements
    append!(outer.cells, inner.cells .+ outer.cells[index])
    append!(outer.blocks, inner.blocks)
    append!(outer.weights, inner.weights .* outer.weights[index])
    # remove the prior cell
    popat!(outer.cells, index)
    popat!(outer.blocks, index)
    popat!(outer.weights, index)
end

"""Increment all cells"""
function shift!(sten::Stencil, offset::CartesianIndex)
	sten.cells .+= [offset]
end

"""Remove the zero weight elements of a stencil for a smaller computational footprint"""
function pruneStencil!(sten::Stencil)
    zeroW = findall(x->x==0, sten.cells)
    for e in reverse(zeroW)
        popat!(sten.cells, e)
        popat!(sten.blocks, e)
        popat!(sten.weights, e)
    end
end

"""Combine stencil elements pointing to the same location, and reorder by block and index"""
function cleanupStencil!(sten::Stencil)
    @assert length(sten.cells) == length(sten.blocks) == length(sten.weights)
    # sort by block
    pb = sortperm(sten.blocks)
    sten.cells[:] = sten.cells[pb]
    sten.blocks[:] = sten.blocks[pb]
    sten.weights[:] = sten.weights[pb]
    blocks = unique(sten.blocks)
    for b in blocks
        # sort each block by index
        blkFirst = findfirst(x->x==b, sten.blocks)
        blkLast = findlast(x->x==b, sten.blocks)
        blkInterval = blkFirst:blkLast
        pi = sortperm(sten.cells[blkInterval]) .+ (blkFirst-1)
        sten.cells[blkInterval] = sten.cells[pi]
        sten.blocks[blkInterval] = sten.blocks[pi]
        sten.weights[blkInterval] = sten.weights[pi]
        # find duplicate cells that also share a block
        duplicateIndices = []
        for i in (blkFirst+1):(blkLast)
            if sten.cells[i-1] == sten.cells[i]
                push!(duplicateIndices, i)
            end
        end
        # combine weights of matching cells and remove the duplicate
        for i in reverse(duplicateIndices)
            sten.weights[i-1] += sten.weights[i]
            popat!(sten.cells, i)
            popat!(sten.blocks, i)
            popat!(sten.weights, i)
        end
    end
end

end
