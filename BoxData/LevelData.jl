module LevelDataMod
export LevelData, getIndices, allIndices, ghostIndices
export MappedLevelData, cellLoc, mappedLoc, plotLevel, plotLevelGrid

struct LevelData
    data::Array
    ghosts::Int
    LevelData(T, dim, cells, ghosts) = new(Array{T, dim}(undef, cells.+2*ghosts), ghosts)
end

import Base.getindex
# acts like an array
function Base.getindex(d::LevelData, i...)
    return getindex(d.data, i...)
end

function Base.lastindex(d::LevelData, i...)
    return lastindex(d.data, i...)
end

function Base.setindex!(d::LevelData, i...)
    return setindex!(d.data, i...)
end

# the interior indices
function getIndices(d::LevelData, ghosts::Int=0; nodes::Bool=false)
    R = CartesianIndices(d.data)
    f, l = first(R), last(R)
    I1 = oneunit(f)
    incGhost = d.ghosts-ghosts
    return f+incGhost*I1 : l+(nodes-incGhost)*I1
end

# indices of the entire data
function allIndices(d::LevelData; nodes::Bool=false)
    return CartesianIndices(d.data)
end

# indices of the entire data
function ghostIndices(d::LevelData, ghosts::Int=d.ghosts; nodes::Bool=false)
    R = CartesianIndices(d.data)
    f, l = first(R), last(R)
    I1 = oneunit(f)
    incGhost = d.ghosts-ghosts
    # FIXME for more than 1D
    return [f+incGhost*I1 : f+(d.ghosts+nodes-1)*I1,
            l-(d.ghosts+1)*I1 : l+(nodes-incGhost)*I1]
end

# functions for levels
function cellLoc(cartIdx, node::Bool=false)
    offset = if !node 0.5 else 0 end
    dx = 1
    return reduce(vcat, [(collect(ci.I) .+ offset).*dx for ci in cartIdx])
end

struct MappedLevelData
    level::LevelData
    mapping::Function
end

function mappedLoc(cartIdx, mapping::Function, node::Bool=false)
    return reduce(vcat, mapping.(cellLoc(cartIdx, node)))
end

function mappedLoc(cartIdx, mld::MappedLevelData, node::Bool=false)
    return reduce(vcat, mld.maping.(cellLoc(cartIdx, node)))
end

function plotLevelGrid(nodes;
                       markScale=1,
                       levLabel="")
    # plot the grid lines
    y = 0*nodes .+ .01
    plot!(nodes, y,
          label = :none,
          linecolor = :black,
          markercolor = :black,
          markersize = 20*markScale,
          markershape = :vline,
          primary = false)
end

function plotLevel(level,
                   plotInterval=getIndices(level);
                   markScale=1,
                   levLabel="")
    # plot the cell centers
    cells = [mappedLoc(i, level.dx) for i in plotInterval]
    plot!(cells, level[plotInterval],
          label = levLabel*" solution",
          markersize = 8*markScale,
          markershape = :circle)
end

end #module
