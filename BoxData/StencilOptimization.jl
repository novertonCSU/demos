using BenchmarkTools

# problem setup
Int dim = 2
Int Isize = 64
SolType = Float64
# data indecies
if dim == 1
    box = CartesianIndices((1:Isize))
elseif dim == 2
    box = CartesianIndices((1:Isize), (1:Isize))
elseif dim == 3
    box = CartesianIndices((1:Isize), (1:Isize), (1:Isize))
else
    error("Not setup for dim > 3")
end

# data to apply stencils on
data = Array{SolType, dim}(undef, size(box))

# --- Test using vanilla stencils ---
# Stencils consisting of neighbors and weights
Stencil = Dict{}
function getStencilElement(stencil::Stencil, index::CartesianIndex)::SolType
    return get(stencil, index, 0)
end
function setStencilElement!(stencil::Stencil, index::CartesianIndex, weight::SolType)
    get!(stencil, index, weight)
end

# Stencils are computed on the fly
function makeStencil(index::CartesianIndex, region::CartesianIndices, )::Stencil
    s = new(stencil)
    return
end

function evalStencil(stencil::Stencil, center::CartesianIndex, work)::SolType
    val = 0
    for (index, weight) in stencil
        val += work(center + index) * getStencilElement(stencil, index)
    end
    return val
end

# Each cell has a cached stencil, sharing when possible

# Each cell has a cached stencil, sharing based on tiling

# Extract all neighbors and use a dot product

# --- Test using cell contributions ---
# Each cell has its set stencil contributions and weights
# Individual stencils get expensive, but in bulk are they cheaper?
