## Given a line, get normal and tangent vectors
using LinearAlgebra
using Plots
#pyplot()
gr()
plot(xlim=(0, 2), ylim=(0, 2), size= (600,600))
#plot()

function pv(vect, origin)
    c = hcat(origin, vect.+origin)'
    return c[:,1], c[:,2]
end

function mag(vect)
    return sqrt(dot(vect, vect))
end

# Define a line using a vector
theta = 1*pi/6.
line = [cos(theta), sin(theta)]
line /= mag(line)
@show line
origin = [0, 0]

plot!(pv(line, origin))

# define a point
pt = [.6, 1.6]
@show mag(pt)
plot!(pv(pt, origin), markershape = :circle)

# the tangent component
@show proj = dot(pt, line)*line/dot(line, line)
@show mag(proj)
plot!(pv(proj, origin), markershape = :circle)

# the tangent component
tan = pt - proj
@show mag(tan)

plot!(pv(tan, origin+proj), markershape = :square)

@show mag(proj)^2 + mag(tan)^2 == mag(pt)^2
# show plots
gui()
