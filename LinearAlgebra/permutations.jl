function permutate(str::T)::Vector{T} where T
    if length(str) == 1
        return [str]
    else
        local perms::Vector{T} = []
        sizehint!(perms, factorial(length(str)))
        for i in eachindex(str) #1:length(str)
            local left = str[i:i] # the i element is left most
            for subPerm in permutate(str[1:i-1] * str[i+1:end]) # skip i element
                push!(perms, left * subPerm)
            end
        end
        return perms
    end
end

@time perms = permutate("abcdefgh") # run the perumations
@time perms = permutate("abcdefgh") # run a second time, since it is now compiled are hopefully faster
#println(perms)
println("Did ", length(perms), " permutations")

# Not setup for arrays
# @time permsInt = permutate([1,2,3,4]) # run the perumations
# @time permsInt = permutate([1,2,3,4]) # run a second time, since it is now compiled are hopefully faster
# #println(perms)
# println("Did ", length(perms), " permutations")
