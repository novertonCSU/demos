function bandedMatrix(band::Vector{T}, size::Int, periodic::Bool=false)::Array{T, 2} where T
    B = zeros(T, size, size)
    width = Int(floor((length(band)-1) / 2))
    begFull = 1+width
    endFull = size-width
    local colBand = reverse(band)
    # full bands (by column)
    for c in begFull:endFull
        B[c-width:c+width, c] = colBand
        #B[c, c-width:c+width] = band # row based
    end
    # partial bands on ends
    for c in 1:begFull-1
        B[1:c+width, c] = colBand[begFull-c+1:end]
        if periodic
            B[end-begFull+c+1:end, c] = colBand[1:begFull-c]
        end
    end
    for c in endFull+1:size
        B[c-width:end, c] = colBand[1:size-c+width+1]
        if periodic
            B[1:c-endFull, c] = colBand[size-c+width+2:end]
        end
    end
    return B
end

N = 10
b = Float64[1.5,0,-2,0,3]
display(bandedMatrix(b, N))
println()
display(bandedMatrix(b, N, true))

N = 10000
println("\nTiming for N=", N, " maxticies")
@time B = bandedMatrix(b, N)
