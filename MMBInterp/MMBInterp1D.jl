# == Include other code ==
push!(LOAD_PATH, pwd()*"/../LeastSquares")
include("../LeastSquares/LinearLS.jl")
using SparseArrays
using DelimitedFiles
using BasisFunctions
const BF = BasisFunctions

# == Run parameters ==
dim = 1
order = 4
connectPt = 0.
nCells = 8 # per grid
nGhosts = 5

Float = Float64
@enum Side lo=0 hi=1

# == Setup mapped grids ==
abstract type Grid end
# struct Grid
#     domainXi::Array{Float, dim}
#     ghostSide::Side
#     name::String
# end
# generic functions to specify
function ghostSide(grid::Grid)::Side
    error("Define specific grid")
end
function domainXi(grid::Grid)
    error("Define specific grid")
end
function domainXiLeng(grid::Grid)
    error("Define specific grid")
end
function X(grid::Grid, xi)
    error("Define specific grid")
end
function dX(grid::Grid, xi)
    error("Define specific grid")
end
function ddX(grid::Grid, xi)
    error("Define specific grid")
end
function I_X(grid::Grid, xi)
    error("Define specific grid")
end
function Xi(grid::Grid, x)
    error("Define specific grid")
end


# monomial mappings X = coef*(xi + off)^p
struct MonoGrid<:Grid
    domainXi::Array{Float, dim}
    ghostSide::Side
    name::String
    p::Float
    off::Float
    coef::Float
end
function ghostSide(g::MonoGrid)::Side
    return g.ghostSide
end
function domainXi(g::MonoGrid)
    return g.domainXi
end
function domainXiLeng(g::MonoGrid)::Float
    return g.domainXi[2]-g.domainXi[1]
end
function X(g::MonoGrid, xi)
    return g.coef*(xi.+g.off).^g.p
end
function dX(g::MonoGrid, xi)
    return g.coef*g.p*(xi.+g.off).^(g.p-1)
end
function ddX(g::MonoGrid, xi)
    return g.coef*g.p*(g.p-1)*(xi.+g.off).^(g.p-2)
end
function I_X(g::MonoGrid, xi)
    return g.coef*((xi.+g.off).^(g.p+1))/(g.p+1)
end
function Xi(g::MonoGrid, x)
    return (x./g.coef).^(1.0/g.p).-g.off
end

# ## Tangent
# sf = 1.5 # streching factor
# a = sf*(pi/4)*2
# b = length/2
# c = (2*tan(sf*(pi/4)))
# mapping(xi) = tan.(a.*(xi .- b))./c .+ b
# mappingD(xi) = (sec.(a*(xi - b)).^2)*a/c
# mappingDD(xi) = (tan.(a*(xi - b)).^2).*(sec.(a*(xi - b)).^2)*(a^2)/c
# mappingI(xi) = -log.(cos.(a*(xi - b)))/(a*c) + xi*b
# mapName="Tan"

# == Setup data storage type ==
struct DataLevel
    data::Array
    grid::Grid
    dXi::Float
    interior::CartesianIndices
    ghost::CartesianIndices
    DataLevel(grid::Grid, a_nCells::Int, a_nGhosts::Int) = new(Array{Float, dim}(undef, a_nCells+a_nGhosts),
                                                               grid,
                                                               domainXiLeng(grid)/a_nCells,
                                                               CartesianIndices((1:a_nCells,)) .+ Bool(ghostSide(grid))*CartesianIndex(a_nGhosts),
                                                               CartesianIndices((1:a_nGhosts,)) .+ !Bool(ghostSide(grid))*CartesianIndex(a_nCells))
end

import Base.getindex
# acts like an array
function Base.getindex(d::DataLevel, i...)
    return getindex(d.data, i...)
end

function Base.lastindex(d::DataLevel, i...)
    return lastindex(d.data, i...)
end

function Base.setindex!(d::DataLevel, i...)
    return setindex!(d.data, i...)
end

function cellXi(level::DataLevel, index, node=false)
    offset = if !node 0.5 else 0 end
    return (index .- 1 .+ offset).*level.dXi
end

function cellXi(level::DataLevel, index::CartesianIndex, node=false)
    offset = domainXi(level.grid)[1] .- first(level.interior).I .+ if !node 0.5 else 0 end
    return collect((index.I .+ offset).*level.dXi)
end

function cellX(level::DataLevel, index, node=false)
    return X(level.grid, cellXi(level, index, node))
end

function cellIdx(level::DataLevel, xi)
    offset = domainXi(level.grid)[1] .- first(level.interior).I
    return CartesianIndex(round.(Int, xi[1]./level.dXi .- offset))
end

function growInterval(intv, grow)
    return intv.start-grow:intv.stop+grow
end

# Analytic <J>
function setJ!(J)
    for i in eachindex(J.data)
        J[i] = (cellX(i+1, J.dx, true)
                - cellX(i, J.dx, true))/(J.dx)
    end
end

# 2nd order d/dx <J> = d/dx J
function setdJ!(dJ)
    for i in eachindex(dJ.data)
        dJ[i] = mappingDD(cellLoc(i, dJ.dx, false))
    end
end

# 2nd order d/dx <U>, at 2nd order <U> = U
function solvedU2!(dU, U, interval=1:size(U.data,1))
    for i in growInterval(interval, -1)
        dU[i] = (U[i+1] - U[i-1])/(2*U.dx)
    end
    i = interval.start
    dU[i] = (-3*U[i] + 4*U[i+1] - U[i+2])/(2*U.dx)
    i = interval.stop
    dU[i] = -(-3*U[i] + 4*U[i-1] - U[i-2])/(2*U.dx)
end

function convolution!(Uavg, U, dU, dx, interval=1:size(U.data,1))
    for i in interval
        Uavg[i] = U[i] + (dx^2/24)*dU[i]
    end
end

function applyEach!(func, range, inputs...)
    for i in range
        func(inputs...)
    end
end

# == define coarse data ==
function setInitialData(solution::DataLevel, initState)
    center = 0
    if initState == -1
        # A constant
        for i in solution.interior
            solution[i] = 1
        end
    elseif initState == 0
        # Use a step condition
        stepLoc = center
        leftState = 1.0
        rightState = 0.0 #-1.0
        for i in solution.interior
            if cellX(solution, i)[] < stepLoc
                solution[i] = leftState
            else
                solution[i] = rightState
            end
        end
    elseif initState == 1
        # Use a sin wave
        amp = 1.0
        period = mapLength
        for i in solution.interior
            solution[i] = amp*sin(cellX(solution, i)*(2*pi/period))
        end
    elseif initState == 2
        # Saw tooth
        amp = 2.0
        period = mapLength
        offset = 0 #-1.0
        for i in solution.interior
            solution[i] = amp*mod(cellX(solution, i),(period/2)) + offset
        end
    elseif initState == 3
        # Smoothed step
        stepLoc = (mapBounds[2] + mapBounds[1])/2
        amp = 0.5
        offset = 0.5
        period = 0.2
        for i in solution.interior
            solution[i] = -amp*tanh((cellX(solution, i) - stepLoc)*(2*pi/period)) + offset
        end
    elseif initState == 4
        # Gaussian
        amplitude = 1
        stdDev = 3
        for i in solution.interior
            solution[i] = amplitude.*exp.(-(cellX(solution, i)[1].-center).^2 ./(2*stdDev^2))
        end
    elseif initState == 5
        # Discontinuous piecewise polynomial
        amp = 0.5 #1.0
        period = (mapBounds[2] + mapBounds[1])/2
        offset = 0.5 #0
        order = 3
        unitLen = 2
        pp = BF.LinearFunction(BF.MonomialBasis(order))
        pp.coef[:] = zeros(order+1)
        pp.coef[end] = 1
        for i in solution.interior
            # point valued
            # x = mod(cellLoc(i, solution.dx), period)*(unitLen/period) - unitLen/2
            # solution[i] = amp*BF.eval(pp, x)
            # average valued
            x = mod.(mappedLoc([i, i+1], solution.dx, true),
                     period+eps()).*(unitLen/period) .- unitLen/2
            solution[i] = amp * BF.integral(pp,
                                            x[1],
                                            x[2])/(solution.dx*(unitLen/period)) .+ offset
        end
    elseif initState == 6
        # Point
        for i in solution.interior
            solution.data[i] = 0
        end
        # solution.data[round(Int, nCells/2)] = 1
        solution.data[11] = 1
    end
end

# == interpolate the ghosts between MMB regions ==
# Using 4th order interp and 7 pt stencil
function MMBInterpolation!(U_l::DataLevel, U_r::DataLevel,
                           trfmLtoR::Function,
                           trfmRtoL::Function;
                           interpType=0::Int, bounded=false)
    if interpType == 0 # Simple copy
        for lg in U_l.ghost
            U_l[lg] = U_r[trfmLtoR(lg)]
        end
        # for rg in U_r.ghost
        #     U_r[rg] = U_l[trfmRtoL(rg)]
        # end
    elseif interpType == 1 # Standard Least Squares as in Chombo
        i1 = oneunit(first(U_l.interior))
        stenSize = -3:3
        for lg in U_l.ghost
            # Create stencil
            x_g = cellX(U_l, lg) # cell center
            xi_g = Xi(U_r.grid, x_g) # inverse
            v_g = cellIdx(U_r, xi_g) # inverse index
            # allocate stencil
            stencilElm = Array{typeof(i1), 1}(undef, length(stenSize))
            stencilBlk = Array{DataLevel, 1}(undef, length(stenSize))
            # fill stencil
            for i in eachindex(stenSize)
                e = stenSize[i]*i1+v_g
                if (e in U_r.interior)
                    stencilElm[i] = e
                    stencilBlk[i] = U_r
                else
                    stencilElm[i] = trfmRtoL(e)
                    stencilBlk[i] = U_l
                end
            end
            @show stencilElm
            @show [blk == U_r for blk in stencilBlk]
            # Interp stencils
            U_l[lg] = U_r[trfmLtoR(lg)]
        end
    elseif interpType == 2 # WLS
    elseif interpType == 3 # IRLS
    else
        error("Invalid interpType "*interpType)
    end
end

# == Plotting ==
using Plots
pyplot()
#gr()
plot()
#plot(xlim = (mapping(0),mapping(1)))
#plot(ylim = (-1.5,1.5))
#plot(ylim = (-0.5,1.5))

### === The "main" program, where everything get called from ===
# initialize the interior solutions
solutionProfile = 4

# solution on block a
grid_a = MonoGrid([-8, 0], lo, "cartesianA", 1, 0, 1)
U_a = DataLevel(grid_a, nCells, nGhosts)
setInitialData(U_a, solutionProfile)
# plot grid
i1 = oneunit(first(U_a.interior))
iNodes_a = vcat([cellX(U_a, i, true) for i in first(U_a.interior):last(U_a.interior.+i1)]...)
plot!(iNodes_a, 0*iNodes_a.+.01, label=:none,
      seriescolor = :blue,
      markersize = 20,
      markershape = :vline,
      primary = false)
gNodes_a = vcat([cellX(U_a, i, true) for i in first(U_a.ghost):last(U_a.ghost.+i1)]...)
plot!(gNodes_a, 0*gNodes_a.+.01, label=:none,
      seriescolor = :blue,
      linestyle = :dash,
      markersize = 20,
      markershape = :vline,
      primary = false)

# solution on block b
grid_b = MonoGrid([0, 10], hi, "cartesianB", 1, 0, 1)
U_b = DataLevel(grid_b, nCells, nGhosts)
setInitialData(U_b, solutionProfile)
# plot grid
i1 = oneunit(first(U_b.interior))
iNodes_b = vcat([cellX(U_b, i, true) for i in first(U_b.interior):last(U_b.interior.+i1)]...)
plot!(iNodes_b, 0*iNodes_b.-.01, label=:none,
      seriescolor = :red,
      markersize = 20,
      markershape = :vline,
      primary = false)
gNodes_b = vcat([cellX(U_b, i, true) for i in first(U_b.ghost):last(U_b.ghost.+i1)]...)
# plot!(gNodes_b, 0*gNodes_b.-.01, label=:none,
#       seriescolor = :red,
#       linestyle = :dash,
#       markersize = 20,
#       markershape = :vline,
#       primary = false)

# interpolate ghost cells
MMBInterpolation!(U_a, U_b,
                  l->l-last(U_a.interior)+first(U_b.interior)-i1,
                  r->r-first(U_b.interior)+last(U_a.interior)+i1,
                  interpType=1)

# plot solutions
iCells_a = vcat([cellX(U_a, i) for i in U_a.interior]...)
plot!(iCells_a, U_a.data[U_a.interior], label="Interior A",
      seriescolor = :blue,
      markershape = :square,
      markersize = 5)

iCells_b = vcat([cellX(U_b, i) for i in U_b.interior]...)
plot!(iCells_b, U_b.data[U_b.interior], label="Interior B",
      seriescolor = :red,
      markershape = :square,
      markersize = 5)

gCells_a = vcat([cellX(U_a, i) for i in U_a.ghost]...)
plot!(gCells_a, U_a.data[U_a.ghost], label="Ghosts A",
      seriescolor = :blue,
      linestyle = :dot,
      markershape = :o)

# gCells_b = vcat([cellX(U_b, i) for i in U_b.ghost]...)
# plot!(gCells_b, U_b.data[U_b.ghost], label="Ghosts B",
#       seriescolor = :red,
#       linestyle = :dot,
#       markershape = :o)

# # write the grids
# open("coar"*mapName*"Grid.dat", "w") do io
#     gridPts = [mappedLoc(i, coarU.dx, true) for i in 1:nCells+1]
#     writedlm(io, gridPts)
# end
# open("fine"*mapName*"Grid.dat", "w") do io
#     gridPts = [mappedLoc(i, fineU.dx, true) for i in 1:refRatio*nCells+1]
#     writedlm(io, gridPts)
# end

gui()

# ### Paper results. 5pt LS with high-order clipping and redistribution
# for case in [0, 2, 3, 4, 5, 6]
#     setInitialData(coarU, case)
#     solvedU2!(coardU, coarU)
#     #JUfromU4!(coarJU, coarJ, coarU, coardJ, coardU)
#     JUfromU2!(coarJU, coarJ, coarU)
#     open("coarData"*mapName*string(case)*".dat", "w") do io
#         # cellPts = [cellLoc(i, coarU.dx) for i in eachindex(coarU.data)]
#         cellPts =  [mappedLoc(i, coarU.dx, false) for i in eachindex(coarU.data)]
#         writedlm(io, hcat(cellPts, coarU.data))
#     end
#     validFineI = fineInterpolation!(coarJU, fineJU, coarJ, fineJ, coarU, fineU, coardJ, finedJ, order; interpType=3, stencilExtra=0, weighting=false)
#     # fCellsI = [cellLoc(i, fineU.dx) for i in validFineI]
#     fCellsI = [mappedLoc(i, fineU.dx, false) for i in validFineI]
#     writeDataI = hcat(fCellsI, fineU.data[validFineI])
#     (minBndI, maxBndI) = clipRedist!(coarU, fineJU, fineU, fineJ, finedJ, finedU, validFineI; clipType=2, stencilRadius=2)
#     open("fineData"*mapName*string(case)*".dat", "w") do io
#         writedlm(io, hcat(writeDataI, fineU.data[validFineI], minBndI[validFineI], maxBndI[validFineI]))
#     end
# end
