% Checking for L1-minimal least squares stencils for MMB
clear;
% ---------------------
% Case 1: Cartesian, normal grid spacing changes
% ---------------------
N = [3 7];
x = cell(2,2);
[x{1,1}, x{1,2}] = meshgrid(0:N(1)+1, 0:N(2));
alpha = .25; % compression factor in Domain 2
[x{2,1}, x{2,2}] = meshgrid(N(1)+1 + alpha*(0:N(1)+1), 0:N(2));

% Plot the mesh
figure(1);
clf;
colormap('default')
colormap([0 0 0; .8 0 0; 0 .8 0; 0 0 .8; .6 .8 .2;])
s = mesh(x{1,1}, x{1,2}, zeros(size(x{1,1})), zeros(size(x{1,1})));
hold on;
s = mesh(x{2,1}, x{2,2}, zeros(size(x{2,1})), zeros(size(x{2,1}))+1);

% Calculate the moments from the MMB boundary face center in real space
x0 = [4 3.5];
% cells to include in Domain 1 (left) or 2 (right) stencils:
%     x | x
%   x x | x x
% x x x o x x x
%   x x | x x
%     x | x
ix1 = [8 11:12 14:16 19:20 24];
ix2 = [5 9:10 13:15 17:18 21]; % cells to include in Domain 2 stencil

P = 3;
M1 = moments(ix1, N(1)+1, P, x{1,1}, x{1,2}, x0(1), x0(2));
M2 = moments(ix2, N(1)+1, P, x{2,1}, x{2,2}, x0(1), x0(2));

% Least squares system for the coefficients in real space
M = [M1; M2]; % Note: rows = 18 points in stencil, columns = 10
C = pinv(M);

% Test it for a polynomial with these coefs
c = [0 0 0 1 1 1 0 1 1 0]';
assert(norm(C*M*c - c,'inf') < 1e-14);

% Calculate the stencil for the face-avg on the MMB
s1 = zeros(N+[1 0]);
s2 = s1;
% f = [1 0 0 0 0 0 0 1/12 0 0]';
f = [1 0 0 0 0 0 0 0 0 0]';
s = pinv(M')*f;
s1(ix1) = s(1:size(ix1,2));
s2(ix2) = s(size(ix1,2) + (1:size(ix2,2)));
no_weight = [s1' s2'] % display the stencils, note it is spread out and not symmetric

% --> Try again with weighting, using the distance^-4
dsq = M(:,2).^2 + M(:,5).^2;
w = min(1,dsq.^-2);
s = w.*pinv(M'*diag(w))*f;
s1(ix1) = s(1:size(ix1,2));
s2(ix2) = s(size(ix1,2) + (1:size(ix2,2)));
dist_weight = [s1' s2'] % note it is spread out and not symmetric

% --> Try again with Manhattan distance^-4
w = [3 3 2 3 2 1 3 2 3]'; % domain 1
w = [w; [3 2 3 1 2 3 2 3 3]'].^-4;
s = w.*pinv(M'*diag(w))*f;
s1(ix1) = s(1:size(ix1,2));
s2(ix2) = s(size(ix1,2) + (1:size(ix2,2)));
manh_dist_weight = [s1' s2'] % note it is tighter in y direction

% --> Try again with IRLS
maxiter=20;
w = ones(18,1)
for iter=1:maxiter
    wnew = w .* abs(s) / max(abs(s));
    %wnew = abs(s).^(1/2) / max(abs(s));
    wnew = round(wnew,10);
    w = wnew;
    snew = w.*pinv(M'*diag(w))*f;    
    if (norm(s - snew, 'inf') < 1e-14) % exit if not changing much        
        iter = iter-1
        break
    end
    s = snew;
end
s1(ix1) = s(1:size(ix1,2));
s2(ix2) = s(size(ix1,2) + (1:size(ix2,2)));
l1_stencil = [s1' s2'] % note it is tighter in y direction

function M = moments(ix, Nx, P, x, y, x0, y0)
jlo = floor((ix-1) / Nx)+1;
ilo = ix - (jlo-1) * Nx;
n = size(ilo,2);
np = (P+1)*(P+2)/2; % number of polynomial coefficients
M = zeros(n,np); 
for i=1:n
    dxlo = x(jlo(i), ilo(i)) - x0;
    dxhi = x(jlo(i), ilo(i)+1) - x0;
    dylo = y(jlo(i), ilo(i)) - y0;
    dyhi = y(jlo(i)+1, ilo(i)) - y0;
    v = (dxhi - dxlo)*(dyhi - dylo);
    p = 0;
    for py = 0:P
    for px = 0:P
        if (px + py > P)
            continue
        end
        mxp = (dxhi^(px+1) - dxlo^(px+1))/(px+1);
        myp = (dyhi^(py+1) - dylo^(py+1))/(py+1);
        M(i, p+1) = mxp*myp/v;
        p = p+1;
    end
    end
end

end