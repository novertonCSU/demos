# == Include other code ==
push!(LOAD_PATH, pwd()*"/../LeastSquares")
include("../LeastSquares/LinearLS.jl")
push!(LOAD_PATH, pwd()*"/../BoxData")
include("../BoxData/LevelData.jl")
include("../BoxData/Stencils.jl")
# using SparseArrays
# using DelimitedFiles
using BasisFunctions
const BF = BasisFunctions
using .LevelDataMod
using .Stencils
using DifferentialEquations
using Plots
gr()

# == Run parameters ==
Float = Float64
dim = 1
domLength = 2.
nCells = 16
nGhost = 6

# dx = domLength/nCells
# cfl = 0.5
# dt = cfl*dx
# stopTime = 0.5 # profile centered at .75
numIter = 1 #ceil(Int, stopTime/dt)
#println("dx: ", dx, "\t dt: ", dt, "\t numIter: ", numIter)

# blocks
sol = Vector{LevelData}(undef, 2)
# == Setup left side data structures ==
sol[1]= LevelData(Float, dim, nCells, nGhost)
solLIndices = getIndices(sol[1])
solLNodeIndices = getIndices(sol[1], nodes=true)
xiOrgL = collect(last(solLNodeIndices).I)
scaleL = .1 #.75
mappingL(xi) = scaleL*(xi.-xiOrgL)
solLCells = mappedLoc(solLIndices, mappingL)
solLNodes = mappedLoc(solLNodeIndices, mappingL, true)

# == Setup right side data structures ==
sol[2] = LevelData(Float, dim, nCells, nGhost)
solRIndices = getIndices(sol[2])
solRNodeIndices = getIndices(sol[2], nodes=true)
xiOrgR = collect(first(solRNodeIndices).I)
scaleR = .1
mappingR(xi) = scaleR*(xi.-xiOrgR)
solRCells = mappedLoc(solRIndices, mappingR)
solRNodes = mappedLoc(solRNodeIndices, mappingR, true)
# ghost cells
solRGIndices = ghostIndices(sol[2])[1] # the low side
solRGNodeIndices = ghostIndices(sol[2], nodes=true)[1]
solRGCells = mappedLoc(solRGIndices, mappingR)
solRGNodes = mappedLoc(solRGNodeIndices, mappingR, true)

# == create the interpolation stencils
# function makeStencil(fromCells, toCells, fromNodeLocs, toNodeLocs;
#                      interpType=0, stencilWidth=0, order=4, weighting=true)
interpType=2; weightType=0; stenWidth=3; order=4;
#     assert(stencilWidth >= cld(order,2))
stenSize = 2*stenWidth + 1
stencils = [Stencil(stenSize) for i in 1:length(solRGIndices)]
for (sten, ghost)  in zip(stencils, solRGIndices)
    println("\n--- ghost ", ghost)
    # make the footprint
    @show sCent = mappedLoc([ghost], mappingR) # valid center
    @show sCentIdx = argmin(abs.(solLCells .- sCent))
    sNodes = []
    lsidx = sCentIdx-stenWidth : min(sCentIdx+stenWidth, length(solLIndices))
    sten.cells[1:length(lsidx)]  = solLIndices[lsidx] # add low side cells
    sten.blocks[1:length(lsidx)] = 2*ones(size(lsidx))
    # @show rsidx = (length(solRIndices) - ((2*stenWidth+1)-length(lsidx)) + 1) : (length(solRIndices))
    rsidx = 1 : (stenSize-length(lsidx))
    @assert (length(lsidx) + length(rsidx)) == stenSize
    sten.cells[length(lsidx)+1:end]  = solRIndices[rsidx] # add high side cells
    sten.blocks[length(lsidx)+1:end] = 1*ones(size(rsidx))
    sNodes = vcat(solLNodes[lsidx.start : lsidx.stop+1], solRNodes[rsidx.start+1 : rsidx.stop+1])
    @assert length(sNodes) == (stenSize+1)
    # solve weights
    polyFit = BF.LinearFunction(BF.MonomialBasis(order-1))
    # moments divided by cell volume
    M = makeLinearMomentLS(polyFit, sNodes .- sCent)./(1)
    # # Adjust to be "balanced" moments with zero value about the cell center
    # K(q) = (mod(q,2)==0 && q>0) ? 1 ./((2 .^ q).*(q .+ 1)) : 0
    # M .-= [K(q) for q in (0:order-1)]'
    # Weight matrix
    W = I # weighting == 0
    if weightType == 1   # Poly inverse to cell center (Hans weighting)
        #W = Diagonal([x -> min(1, (1/(abs(x))).^(order/2)), normStencil])
        W = Diagonal( [min(1, abs(e)^(-(order+1))) for e in M[:,2]] )
    elseif weightType == 2  # inverse manhattan distance
        W = Diagonal( (abs.(-stenWidth:stenWidth) .+ 1.).^(-(order+1)) )
    elseif weightType == 3 # hybrid manhattan/poly inversse
    end
    display(M)
    display(W)
    # moments of the ghost cell
    gmom = reshape(makeLinearMomentLS(polyFit,
                                      mappedLoc(ghost:ghost+oneunit(ghost), mappingR, true)
                                      .- sCent)./(1), :)
    g = gmom # evaluation vector for the cell average
    @show g
    if interpType == 0 # constant
        sten.weights[findfirst(x->x==sCentIdx, lsidx)] = 1
    elseif interpType == 1 # vanilla LS - as in McCorquadale paper
        sten.weights[:] = g * pinv(W*M)*W
    elseif interpType == 2 # IRLS
        sten.weights[:] = IRLS(M', g) # update weights
    end
    display(sten)
    @assert sum(sten.weights) ≈ 1
end

#     return stencils
# end

# == set the exact profile ==
function setExact(sol, solMapping; state=0)
    for i in allIndices(sol)
        if state==0
            sol[i] = 1
        elseif state==1
            # Gaussian
            cent = -0.5
            amplitude = 1.
            stdDev = 0.1 #0.04
            sol[i] = amplitude.*exp.(-(mappedLoc([i], solMapping)[1].-cent).^2 ./ (2*stdDev^2))
            if sol[i] < eps(1.)
                sol[i] = 0
            end
        else
            @assert false
        end
    end
end

# == setup the advection problem ==
function makeAdvectionStencil(dx, dim; spaceType=0)::Stencil
    sten = Stencil(0)
    p1 = CartesianIndex(1)
    # define the fluxes (on the low side)
    if spaceType == 0 # second order centered
        sten = Stencil(2)
        sten.cells[:] = [-1*p1, 0*p1]
        sten.weights[:] = [1, 1]./(2*dx)
    elseif spaceType == 1 # fourth order centered
        sten = Stencil(4)
        sten.cells[:] = [-2*p1, -1*p1, 0*p1, 1*p1]
        sten.weights[:] = [-1, 7, 7, -1]./(12*dx)
    elseif spaceType == 2 # first order upwind (left)
        sten = Stencil(1)
        sten.cells[:] = [-1*p1]
        sten.weights[:] = [1]./(1*dx)
    elseif spaceType == 3 # second order upwind (left)
        sten = Stencil(3)
        sten.cells[:] = [-2*p1, -1*p1, 0*p1]
        sten.weights[:] = [-1, 4, 1]./(4*dx)
    elseif spaceType == 4 # third order upwind (left)
        sten = Stencil(3)
        sten.cells[:] = [-2*p1, -1*p1, 0*p1]
        sten.weights[:] = [-1, 5, 2]./(6*dx)
    end
    # combines for a cell update
    stenL = deepcopy(sten)
    stenR = deepcopy(sten)
    shift!(stenR, p1)
    stenR.weights .*= -1
    sten = stenL + stenR
    return sten
end

function makeMMBStencil(opStencil, ghostStencils)
    comboStencils = ghostStencils
    return comboStencils
end

function applyStencilOp!(opval, sol, params, t)
    stencil, region, mmbStencils, mmbRegions = params
    applyStencilForEach!(opval, stencil, [sol], region)
end

function advectAdv!(sol, sten, numIter, dt; timeType=0)
    interior = getIndices(sol)
    opParams = [sten, interior]
    odeForm = ODEProblem(applyStencilOp!, sol[:], (0., dt*numIter), opParams)
    if timeType==-1
        opval = deepcopy(sol)
        for i = 1:numIter
            op(opval[:], sol[:], opParams, spaceType)
            sol[interior] = sol[interior] .+ dt.*opval[interior] # explicit euler
        end
        tpts = dt.*(1:numIter)
    elseif timeType==0
        (sols, tpts) = solve(odeForm, Euler(), dt=dt)
        sol[:] = sols[end]
    elseif timeType==1
        (sols, tpts) = solve(odeForm, Heun(), dt=dt)
        sol[:] = sols[end]
    elseif timeType==2
        (sols, tpts) = solve(odeForm, RK4(), dt=dt)
        sol[:] = sols[end]
    else
        @assert false
    end
    return tpts
end

# == Interpolate ==
for s in stencils
    #applyStencil(s, sol)
end

# == solve the advection problem ==
setExact(sol[1], mappingL; state=1)
setExact(sol[2], mappingR; state=1)

# == Plot solution ==
plot()
gridYoffset = 0.05

y = 0 .* solLNodes# .+ gridYoffset
plot!(solLNodes, y,
      label = :none,
      linecolor = :red,
      markercolor = :red,
      markersize = 20,
      markershape = :vline,
      primary = false)

y = 0 .* solRGNodes# .+ gridYoffset
plot!(solRGNodes, y,
      label = :none,
      linecolor = :cyan,
      markercolor = :cyan,
      markersize = 16,
      markershape = :vline,
      primary = false)

y = 0 .* solRNodes# .- gridYoffset
plot!(solRNodes, y,
      label = :none,
      linecolor = :blue,
      markercolor = :blue,
      markersize = 20,
      markershape = :vline,
      primary = false)

# Plot the initial solution
plot!(vcat(solLCells, solRCells),
      vcat(sol[1][solLIndices], sol[2][solRIndices]),
      linewidth=2, label="Init")

# Solve
advectSten = makeAdvectionStencil(dx, dim; spaceType=0)
tpts = advectAdv!(sol, advectSten, numIter, dt; timeType=0)

# Plot the final solution
plot!(vcat(solLCells, solRCells),
      vcat(sol[1][solLIndices], sol[2][solRIndices]),
      linewidth=2, label="Final")

# # == Make the system as a matrix and show connection ==
# SM = spzeros(nCells, nCells)
# for i in 1:length(solIndices)
#     SM[i,i] = 1
#     for (j, w) in zip(advectSten.cells, advectSten.weights)
#         if (j + solIndices[i]) in solIndices
#             SM[i, j + CartesianIndex(i)] += w
#         end
#     end
# end
# pltMat = spy(SM)
# # Show the eigenvalues for this operator
# ev = eigvals(Matrix(SM))
# pltEigs = scatter(real(ev), imag(ev))

# plot(pltSol, pltMat, pltEigs, layout = (3,1), legend = false)
