#include <iostream>
#include <string>
const size_t sz = 4;

// Takes in a function pointer and its arguments
// Loops over the an array an applies the function to each element
template<typename Func, typename... Srcs>
inline void forall(const Func& a_F, Srcs&... a_srcs)
{
    for (size_t i=0; i!=sz; i++)
    {
    a_F(i, std::forward<Srcs>(a_srcs)...);
    }
}

void incr(size_t i, int* a, int* b)
{
    a[i] += b[i];
}

void printEl(size_t i, int* a)
{
    std::cout << a[i] << ' ';
}

void printArr(int* a, std::string name)
{
  std::cout << name << ": ";
  forall(printEl, a);
  std::cout << std::endl;
}

int main(){
    int A[] = {1,2,3,4};
    int B[] = {1,1,2,2};
    std::cout << "Initial arrays -\n";
    printArr(A, "A");
    printArr(B, "B");
    forall(incr, A, B);
    std::cout << "After incr -\n";
    printArr(A, "A");
    printArr(B, "B");
    // for (size_t i=0; i!=sz; i++)
    // {
    //     std::cout << a[i] << '\t';
    //     std::cout << b[i] << '\n';
    // }
}
