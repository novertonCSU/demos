module BasisFunctions

#=

=#

export FunctionBasis, PolyBasis, MonomialBasis, PolyTensorProductBasis
export evalBasis, evalBasisDeriv, evalBasisIntegral,
    derivativeBasis, integralBasis, numBases
export GenericFunction, LinearFunction
export eval, derivative, integral
export evalFuncs, derivativeFuncs, integralFuncs

abstract type FunctionBasis end
# Variations of polynomials. Each can be converted to one another
abstract type PolyBasis<:FunctionBasis end
struct MonomialBasis<:PolyBasis
    order::Int
end
struct BernsteinBasis<:PolyBasis
    order::Int
end
struct HermiteBasis<:PolyBasis
    order::Int
end
struct ChebeshevBasis<:PolyBasis
    order::Int
end
# multi-dimension polynomials
struct PolyTensorProductBasis<:PolyBasis
    order::Vector{Int}
    bases::Vector{PolyBasis}
end
# Other bases
abstract type FourierBasis<:FunctionBasis end
abstract type WaveletBasis<:FunctionBasis end
abstract type RootBasis<:FunctionBasis end
abstract type RationalBasis<:FunctionBasis end
abstract type ExpBasis<:FunctionBasis end
abstract type PowerBasis<:FunctionBasis end

# ------------------------------------------------------------------------------
# Generic methods for a basis, which must be overwritten
function evalBasis(basis::FunctionBasis, x)::Array{Number}
    error("Basis evaluation is specific to the basis")
end

function evalBasisDeriv(basis::FunctionBasis, x, grad)::Array{Number}
    error("No expressible derivative exists for generic functions")
end

function evalBasisIntegral(basis::FunctionBasis, xLo, xHi)::Array{Number}
    error("No expressible integral exists for generic functions")
end

function derivativetBasis(basis::FunctionBasis)::FunctionBasis
    error("No expressible derivative exists for generic functions")
end

function integralBasis(basis::FunctionBasis)::FunctionBasis
    error("No expressible integral exists for generic functions")
end

function numBases(basis::FunctionBasis)::Int
    return sum(basis.order .+ 1)
end

function dimBases(basis::FunctionBasis)::Int
    if isempty(basis.order)
        return 1
    else
        return sum(size(basis.order))
    end
end
# ------------------------------------------------------------------------------
# Polynomials
function evalBasis(T::MonomialBasis, x)::Array{Number}
    return [x.^(b) for b in 0:T.order]
end

function evalBasisDeriv(T::MonomialBasis, x, grad)::Array{Number}
    deriv(x, p, d) = factorial(d).*binomial(p,d).*x.^(p-d)
    return [deriv(x,b,grad) for b in 0:T.order]
end

function evalBasisIntegral(T::MonomialBasis, xLo, xHi)::Array{Number}
    integral(x, p) = (1/(p+1)).*x.^(p+1)
    return [integral(xHi,b)-integral(xLo,b) for b in 0:T.order]
end


# ------------------------------------------------------------------------------
# Types of functions
abstract type GenericFunction end

#= Mathematical functions of the type that operate in a linear function space
   The following properties hold for function F and G
   (F+G)(x) = F(x) + G(x)
   F(x) + G(x) = G(x) + F(x)
   F(x)*G(x) = G(x)*F(x)
   F(c*x) = c*F(x)
   1*F(x) = F(x)
   0*F(x) = 0
=#
struct LinearFunction<:GenericFunction
    basis::FunctionBasis
    coef::Array{Number}
    # LinearFunction(basis::FunctionBasis) = new(basis,
    #                                            Matrix{Number}(undef, numBases(basis), dimBases(basis)))
    LinearFunction(basis::FunctionBasis) = new(basis,
                                               Vector{Number}(undef, numBases(basis)))
end

# evaluate a linear function at a point
function evalFuncs(func::LinearFunction, x)
    return evalBasis(func.basis,x).*func.coef
end

function eval(func::LinearFunction, x)::Number
    return sum(evalFuncs(func, x))
end

# evaluate the gradient of a linear function at a point
function derivativeFuncs(func::LinearFunction, x, grad)
    return evalBasisDeriv(func.basis, x, grad).*func.coef
end

function derivative(func::LinearFunction, x, grad)::Number
    return sum(derivativeFuncs(func, x, grad))
end

# evaluate the integral of a linear function over a region
function integralFuncs(func::LinearFunction, rangeLo, rangeHi)
    return evalBasisIntegral(func.basis, rangeLo, rangeHi).*func.coef
end

function integral(func::LinearFunction, rangeLo, rangeHi)::Number
    return sum(integralFuncs(func, rangeLo, rangeHi))
end


end
