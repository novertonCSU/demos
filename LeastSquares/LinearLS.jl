#module LinearLS
using LinearAlgebra

push!(LOAD_PATH, pwd())
using BasisFunctions
const BF = BasisFunctions

# Make the matrix A for a least squares problem for problem F(x) = y at known
function makeLinearLS(func::BF.LinearFunction, x)::Matrix
    #@assert size(x) == size(y)
    A = Matrix(undef, size(x, 1), BF.numBases(func.basis))
    for r in 1:size(A,1)
        A[r,:] = BF.evalBasis(func.basis, x[r])
    end
    return A
end

function makeLinearMomentLS(func::BF.LinearFunction, ranges)::Matrix
    #@assert size(x) == size(y)
    A = Matrix(undef, size(ranges, 1).-1, BF.numBases(func.basis))
    for r in 1:size(A,1)
        A[r,:] = BF.evalBasisIntegral(func.basis, ranges[r], ranges[r+1])
    end
    return A
end

function makeDiagWeights(func, x)::Matrix
    #n = size(x,1)
    # W = zeros(n,n)
    # for d in 1:n
    #     W[d,d] = func(x[d])
    # end
    #return W
    return Diagonal([func.(x)])
end

"Solve the minimum for a Ax=y problem"
function linearLS(A::Array, y)::Array
    return (A)\(y)
end

"Solve the minimum for a weighted W*(Ax=y) problem"
function linearLS(A::Array, y, W)::Array
    return (W*A)\(W*y)
end

"Solve for x which minimizes the problem Ax=y problem subject to constraint Cx=d "
function linearConstrainedLS(A::Array, y, C::Array, d)::Array
    xsz = size(A, 2)
    @assert size(A,2) == size(C,2)
    zsz = size(d, 1)
    L = [2*transpose(A)*A  transpose(C); C  zeros(zsz, zsz)]
    R = [2*transpose(A)*y; d]
    X = L\R
    return X[1:xsz]
end

"Solve for x which minimizes the problem W*(Ax=y) problem subject to constraint Cx=d "
function linearConstrainedLS(A::Array, y, C::Array, d, W)::Array
    xsz = size(y, 1)
    zsz = size(d, 1)
    L = [2*W*A*transpose(A)  transpose(C); C  zeros(zsz, zsz)]
    R = [2*transpose(A)*W*y; d]
    X = L\R
    return X[1:xsz]
end

"Solve for x with the minimum L_p norm of W*x which satisfies the constraint Ax=b "
function IRLS(A, b; p=1, K=.8, iter=20, ωv=ones(size(A)[2]))
    @assert(p<2)
    pk = 2
    x = pinv(A)*b
    #for i in 1:iter
    i = 1; convg = 0
    while (i <= iter) && (convg<=2)
        i += 1
        # round off everything everything small before finishing
        if convg == 1
            ωv = map(x -> x < eps(1.)*1E4 ? 0 : x, ωv)
            convg += 1
        end
        #pk = max(p, i*pk)
        #ωv = abs.(x).^((2-pk)/2)
        ωv = ωv .* abs.(x)
        ωv /= maximum(abs.(ωv)) # normalize weights
        # consider removing the smallest non-zero element
        min_nze = argmin(abs.(map(x -> x==0 ? 1 : x, ωv)))
        if ωv[min_nze] < eps(1.)*1E4
            ωv[min_nze] = 0
        end
        # make weights into a matrix and solve
        ω = Diagonal(ωv)
        xnew = ω*pinv(A*ω)*b
        # @show ωv
        # @show x
        # @show xnew
        if norm(x-xnew, Inf) < eps(1.)*1E2
            convg += 1
        else
            convg = 0
        end
        x = xnew
    end
    # @show A*x-b
    @assert A*x ≈ b
    return x
end

function approx(A::Array, x)::Array
    return A*x
end

function residual(y_approx::Array, y_exact::Array)::Array
    return y_approx - y_exact
end

function residual(A::Array, x::Array, y::Array)::Array
    y_approx = approx(A,x)
    return residual(y_approx, y)
end

function rms(r::Array)::Number
    return sqrt(sum(r.^2))
end

function predictionError(A::Array, x::Array, y::Array)::Number
    y_approx = approx(A,x)
    r = residual(y_approx, y)
    return rms(r)
end

function relativeError(A::Array, x::Array, y::Array)::Number
    return predictionError(A,x,y)/rms(y)
end

#end
