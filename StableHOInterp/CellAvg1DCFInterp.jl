# == Include other code ==
push!(LOAD_PATH, pwd()*"/../LeastSquares")
include("../LeastSquares/LinearLS.jl")
using SparseArrays
using DelimitedFiles
using BasisFunctions
const BF = BasisFunctions

# == Run parameters ==
dim = 1
length = 1.
nCells = 16
refRatio = 4

Float = Float64

# == Setup data structures ==
struct DataLevel
    data::Array
    dx::Float
    refRatio::Int
end

function makeFinerLevel(coarseLevel::DataLevel,
                        refRatio::Int)::DataLevel
    return DataLevel(
        typeof(coarseLevel.data)(undef, size(coarseLevel.data).*refRatio),
        coarseLevel.dx/refRatio,
        refRatio)
end

coarSol = DataLevel(Array{Float, dim}(undef, nCells),
                    length/nCells, 1)
fineSol = makeFinerLevel(coarSol, refRatio)

function cellLoc(index, dx, node=false)
    offset = if !node 0.5 else 0 end
    return (index .- 1 .+ offset).*dx
end

function fineToCoarseIdx(fineIndex, refRatio)
    return div((fineIndex.-1),refRatio) .+ 1
end

function coarseToFineIdxs(coarseIndex, refRatio)
    startFineIdx = (coarseIndex.-1).*refRatio .+ 1
    return startFineIdx:startFineIdx.+refRatio.-1
end

function growInterval(intv, grow)
    return intv.start-grow:intv.stop+grow
end

function getValidCoarse(validFine, refRatio)
    return Int((validFine.start-1)/refRatio+1):Int((validFine.stop)/refRatio)
end

# == define coarse data ==
function setInitialData(solution, initState)
    if initState == 0
        # Use a step condition
        stepLoc = length/2
        leftState = 1.0
        rightState = 0 #-1.0
        for i in eachindex(solution.data)
            if cellLoc(i, solution.dx)[] < stepLoc
                solution.data[i] = leftState
            else
                solution.data[i] = rightState
            end
        end
    elseif initState == 1
        # Use a sin wave
        amp = 1.0
        period = length
        for i in eachindex(solution.data)
            solution.data[i] = amp*sin(cellLoc(i, solution.dx)*(2*pi/period))
        end
    elseif initState == 2
        # Saw tooth
        amp = 2.0
        period = length
        offset = 0 #-1.0
        for i in eachindex(solution.data)
            solution.data[i] = amp*mod(cellLoc(i, solution.dx),(period/2)) + offset
        end
    elseif initState == 3
        # Smoothed step
        stepLoc = length/2
        amp = 0.5
        offset = 0.5
        period = 0.2
        for i in eachindex(solution.data)
            solution.data[i] = -amp*tanh((cellLoc(i, solution.dx) - stepLoc)*(2*pi/period)) + offset
        end
    elseif initState == 4
        # Gaussian
        cent = length/2
        amplitude = 1.
        stdDev = 0.1
        for i in eachindex(solution.data)
            solution.data[i] = amplitude.*exp(-(cellLoc(i, solution.dx)-cent)^2/(2*stdDev^2))
        end
    elseif initState == 5
        # Discontinuous piecewise polynomial
        amp = 0.5 #1.0
        period = length/2
        offset = 0.5 #0
        order = 3
        unitLen = 2
        pp = BF.LinearFunction(BF.MonomialBasis(order))
        pp.coef[:] = zeros(order+1)
        pp.coef[end] = 1
        for i in eachindex(solution.data)
            # point valued
            # x = mod(cellLoc(i, solution.dx), period)*(unitLen/period) - unitLen/2
            # solution.data[i] = amp*BF.eval(pp, x)
            # average valued
            x = mod.(cellLoc([i, i+1],
                             solution.dx, true),
                     period+eps()).*(unitLen/period) .- unitLen/2
            solution.data[i] = amp * BF.integral(pp,
                                                 x[1],
                                                 x[2])/(solution.dx*(unitLen/period)) + offset
        end
    elseif initState == 6
        # Point
        for i in eachindex(solution.data)
            solution.data[i] = 0
        end
        solution.data[round(Int, nCells/2)] = 1
    end
end

# == Conservation maintenance ==
function conservationCalc(coarseCell, fineCells, report=false)
    consvSum = coar.data[ci]
    for fi in fineCells
        consvSum -= fine[fi]./refRatio
    end
    if report
        println("Cell conservation error = ", consvCheck)
    end
    return consvSum
end

function conservationCheck(coarse, fine, validFine, report=false)
    consvCheck = 0;
    maxValMag = 0;
    for ci in getValidCoarse(validFine, refRatio)
        maxValMag = max(maxValMag, abs(coarse.data[ci]))
        consvCheck = max(consvCheck,
                         conservationCalc(coarse.data[ci],
                                          fine.data[coarseToFineIdxs(ci, refRatio)]))
    end
    conservative = abs(consvCheck) < maxValMag*eps()
    if report
        println("Solution conservation is ", conservative)
        println("  max conservation error = ", consvCheck)
    end
    return conservation
end

function conservationCorrect!(coarse, fine, validFine)
    for ci in validCoarse
        consVal = coarse.data[ci]
        for fi in coarseToFineIdxs(ci, refRatio)
            consVal -= fine.data[fi]./refRatio
        end
        if consVal != 0
            for fi in coarseToFineIdxs(ci, refRatio)
                fine.data[fi] += consVal
            end
        end
    end
end

# == interpolate the fine level ==
function fineInterpolation!(coar, fine, N;
                            interpType=0, stencilExtra=0, weighting=true)
    npolyBaseStenSize = cld(N,2) # stencil extent in each direction not including center
    stencilSize = npolyBaseStenSize + stencilExtra
    validFine = stencilSize*refRatio+1:(nCells-stencilSize)*refRatio
    if interpType == 0
        # 0th order interpolation
        stencilSize = 0
        validFine = 1:refRatio*nCells
        for i in eachindex(fine.data)
            fine.data[i] = coar.data[fineToCoarseIdx(i, refRatio)]
        end
    elseif interpType == 1
        # Nth order point interpolation
        # Do a polynomial (possibly least squares) fit in each coarse cell, eval for each fine contained
        for ci in 1+stencilSize:nCells-stencilSize
            stencil = ci-stencilSize:ci+stencilSize
            y = view(coar.data, stencil)
            x0 = cellLoc(ci, coar.dx)
            z = cellLoc(stencil, coar.dx) .- x0
            polyFit = BF.LinearFunction(BF.MonomialBasis(N-1))
            M = makeLinearLS(polyFit, z)
            polyFit.coef[:] = linearLS(M, y)
            for fi in coarseToFineIdxs(ci, refRatio)
                zf = cellLoc(fi, fine.dx) .- x0
                fine.data[fi] = BF.eval(polyFit, zf)
            end
        end
    elseif interpType == 2
        # Nth order cell average interpolation
        # Do a polynomial (possibly least squares) fit in each coarse cell, eval for each fine contained
        normStencil = -stencilSize:stencilSize
        normStencilNodes = (normStencil.start:normStencil.stop+1) .- 0.5
        polyFit = BF.LinearFunction(BF.MonomialBasis(N-1))
        M = makeLinearMomentLS(polyFit, normStencilNodes)./(1) # moments divided by cell volume
        W = I
        if weighting
            # Weight to furthest cell extent (Nate weighting, works poorly)
            # W = makeDiagWeights(x -> (1/(abs(x)+0.5)).^(N), normStencil)
            # Weight to cell center (Hans weighting)
            W = makeDiagWeights(x -> min(1, (1/(abs(x))).^(N/2)), normStencil)
        end
        println("Order = ", N, "\tStencil size = ", size(normStencil))
        consvCheck = 0;
        for ci in 1+stencilSize:nCells-stencilSize
            # Solve the polynomial for this coarse cell
            y = view(coar.data, normStencil.+ci)
            polyFit.coef[:] = linearLS(M, y, W)
            xi0 = ci + 0.5
            # interpolate fine cells
            consvSum = coar.data[ci]
            for fi in coarseToFineIdxs(ci, refRatio)
                xifCell = ([fi, fi+1] .-1)./refRatio .+ 1 .- xi0
                fine.data[fi] = BF.integral(polyFit,
                                            xifCell[1],
                                            xifCell[2]).*refRatio
                consvSum -= fine.data[fi]./refRatio
            end
            consvCheck = max(consvCheck, abs(consvSum))
        end
        println("max conservation error = ", consvCheck)
    elseif interpType == 3
        # Nth order conservative cell average interpolation
        # Do a polynomial (possibly least squares) fit in each coarse cell, eval for each fine contained
        # See eq 50 of McCorquodale 2011
        normStencil = [collect(-stencilSize:-1); collect(1:stencilSize)]
        normStencilNodes = (-stencilSize:stencilSize+1) .- 0.5
        polyFit = BF.LinearFunction(BF.MonomialBasis(N-1))
        # Make average = 0 in [-0.5, 0.5]
        K(q) = (mod(q,2)==0 && q>0) ? 1 ./((2 .^ q).*(q .+ 1)) : 0
        # moments divided by cell volume
        M = (makeLinearMomentLS(polyFit, normStencilNodes).-[K(q) for q in (0:N-1)]')./(1)
        # remove row and column for conservation constraint
        M = [M[1:stencilSize, 2:end]; M[stencilSize+2:end,2:end]]
        W = I
        if weighting
            # Weight to furthest cell extent (Nate weighting, works poorly)
            # W = makeDiagWeights(x -> (1/(abs(x)+0.5)).^(N), normStencil)
            # Weight to cell center (Hans weighting)
            W = makeDiagWeights(x -> min(1, (1/(abs(x))).^(N/2)), normStencil)
        end
        println("Order = ", N, "\tStencil size = ", size(normStencil))
        consvCheck = 0;
        for ci in 1+stencilSize:nCells-stencilSize
            # conservation constraint enforces constant
            polyFit.coef[1] = coar.data[ci]
            # Solve the polynomial
            y = view(coar.data, normStencil.+ci)
            polyFit.coef[2:end] = linearLS(M, y .- polyFit.coef[1]*1, W)
            xi0 = ci + 0.5
            # interpolate fine cells
            consvSum = coar.data[ci]
            for fi in coarseToFineIdxs(ci, refRatio)
                xifCell = ([fi, fi+1] .-1)./refRatio .+ 1 .- xi0
                fine.data[fi] = BF.integral(polyFit,
                                            xifCell[1],
                                            xifCell[2]).*(refRatio)
                fine.data[fi] -= sum(polyFit.coef .* [K(q) for q in (0:N-1)])
                consvSum -= fine.data[fi]./refRatio
            end
            consvCheck = max(consvCheck, abs(consvSum))
        end
        println("max conservation error = ", consvCheck)
    end
    return validFine
end

# == Filters to use ==
function medianFilter!(data, iter=1)
    sz = prod(size(data))
    F = spdiagm(1 => ones(sz-1)/4, -1 => ones(sz-1)/4, 0 => 2*ones(sz)/4)
    F[1,1] += 1/4
    F[sz,sz] += 1/4
    for i in 1:iter
        data[:] = F*data
    end
    ## Same thing if you wanted to hold onto the matrix
    # F = F^iter
    # data[:] = F*data
end

# == Clipping and Redistribution ==
function clipRedist!(coarse, fine, validFine; redistType=1, clipType=1, iter=1, stencilRadius=1, conservative=true)
    # Get valid coarse
    validCoarse = getValidCoarse(validFine, refRatio)
    # make bounds for the clipping
    minBnd = deepcopy(fine)
    maxBnd = deepcopy(fine)
    enableClipping = fill(true, size(fine.data))
    if clipType >= 1 # 1st order bounds
        normStencil = -stencilRadius:stencilRadius
        for ci in 1+stencilRadius:nCells-stencilRadius
            stenMin = minimum(coarse.data[normStencil.+ci])
            stenMax = maximum(coarse.data[normStencil.+ci])
            for fi in coarseToFineIdxs(ci, refRatio)
                minBnd.data[fi] = stenMin
                maxBnd.data[fi] = stenMax
            end
        end
    end
    if clipType == 2 # High order clipping
        # compute 2nd derivative of each coarse cell
        # Order doesn't matter, just want the sign
        secondDeriv = deepcopy(coarse)
        normStencil = -stencilRadius:stencilRadius
        for ci in 1+stencilRadius:nCells-stencilRadius
            secondDeriv.data[ci] = (coarse.data[ci-1] - 2*coarse.data[ci] + coarse.data[ci+1])/(2*coarse.dx^2)
        end
        @show secondDeriv
        for ci in 1+stencilRadius:nCells-stencilRadius
            smooth = (sign(secondDeriv.data[ci-1]) ==
                      sign(secondDeriv.data[ci]) ==
                      sign(secondDeriv.data[ci+1]))
            for fi in coarseToFineIdxs(ci, refRatio)
                enableClipping[fi] = !smooth
            end
        end
    end
    # smooth the bounds
    medianFilter!(view(minBnd.data, validFine), 4)
    medianFilter!(view(maxBnd.data, validFine), 4)
    # adjust solution to be conservative
    if conservative
        for ci in validCoarse
            consVal = coarse.data[ci]
            for fi in coarseToFineIdxs(ci, refRatio)
                consVal -= fine.data[fi]./refRatio
            end
            if consVal != 0
                for fi in coarseToFineIdxs(ci, refRatio)
                    fine.data[fi] += consVal
                end
            end
        end
    end

    # Track the regions that need clipped
    extraMass = deepcopy(fine)
    for ci in validCoarse
        for fi in coarseToFineIdxs(ci, refRatio)
            extraMass.data[fi] = max(fine.data[fi]-maxBnd.data[fi], 0)
            if extraMass.data[fi] == 0
                extraMass.data[fi] = min(fine.data[fi]-minBnd.data[fi], 0)
            end
        end
    end
    @show extraMass
    # simple clipping, local to each coarse cell
    # Maintaints a conservative solution, but does not check for it
    if redistType == 1
        for ci in validCoarse
            fineCells = coarseToFineIdxs(ci, refRatio)
            toRedist = sum(extraMass.data[fineCells])
            # clip
            @show all(enableClipping[fineCells])
            if all(enableClipping[fineCells])
                for fi in fineCells
                    if fine.data[fi] > maxBnd.data[fi]
                        fine.data[fi] = maxBnd.data[fi]
                    elseif fine.data[fi] < minBnd.data[fi]
                        fine.data[fi] = minBnd.data[fi]
                    end
                end
                # redistribute
                allowance = zeros(size(fineCells))
                if toRedist > 0
                    allowance = max.(maxBnd.data[fineCells] - fine.data[fineCells], 0)
                elseif toRedist < 0
                    allowance = min.(minBnd.data[fineCells] - fine.data[fineCells], 0)
                end
                # there has to be room to redistribute to
                @show abs(sum(allowance))
                @show abs(toRedist)
                # @assert abs(sum(allowance)) <= abs(toRedist) + sqrt(eps(maximum(abs.(allowance))))
                @assert abs(sum(allowance)) >= abs(toRedist) - sqrt(eps(maximum(abs.(allowance))))
                if (sum(allowance) != 0)
                    fine.data[fineCells] += toRedist.*allowance./sum(allowance)
                end
            end
        end
    end
    # Clip, and track extra mass
    # redistribute, assuming a conservative solution


    # return min/max
    return (minBnd, maxBnd)
end

# == Plotting ==
using Plots
#pyplot()
gr()
plot()
#plot(xlim = (0,1),  ylim = (-1.5,1.5))
#plot(xlim = (0,1), ylim = (-0.5,1.5))

function plotLevelGrid(level, plotInterval=eachindex(level.data);
                       markScale=1/level.refRatio,
                       levLabel="")
    # plot the grid lines
    nodes = [cellLoc(i, level.dx, true) for i in plotInterval]
    y = 0*nodes
    plot!(nodes, y,
          label = :none,#levLabel*" grid",
          linecolor = :black,
          markercolor = :black,
          markersize = 20*markScale,
          markershape = :vline,
          primary = false)
end

function plotLevel(level, plotInterval=eachindex(level.data);
                   markScale=1/level.refRatio,
                   levLabel="")
    # plot the cell centers
    cells = [cellLoc(i, level.dx) for i in plotInterval]
    plot!(cells, level.data[plotInterval],
          label = levLabel*" solution",
          markersize = 8*markScale,
          markershape = :circle)
end

### The "main" program, where everything get called from
# initialized the coarse solution
order = 4
setInitialData(coarSol, 5) #3 5

# plot the grids
plotLevelGrid(coarSol, levLabel="coarse")
validFine = 1:refRatio*nCells
plotLevelGrid(fineSol, validFine, levLabel="fine")
plotLevel(coarSol, levLabel="coarse")

# write the grids
open("coarGrid.dat", "w") do io
    gridPts = [cellLoc(i, coarSol.dx, true) for i in 1:nCells+1]
    writedlm(io, gridPts)
end
open("fineGrid.dat", "w") do io
    gridPts = [cellLoc(i, fineSol.dx, true) for i in 1:refRatio*nCells+1]
    writedlm(io, gridPts)
end

### LS
# validFine = fineInterpolation!(coarSol, fineSol, order; interpType=2, stencilExtra=0, weighting=false)
# plotLevel(fineSol, validFine, levLabel="LS, 5pt", markScale=0.5)

# validFine = fineInterpolation!(coarSol, fineSol, order; interpType=2, stencilExtra=1, weighting=false)
# plotLevel(fineSol, validFine, levLabel="LS, 7pt", markScale=0.5)


### Weighted LS
# validFine = fineInterpolation!(coarSol, fineSol, order; interpType=2, stencilExtra=0, weighting=true)
# plotLevel(fineSol, validFine, levLabel="WLS, 5pt", markScale=0.5)

# validFine = fineInterpolation!(coarSol, fineSol, order; interpType=2, stencilExtra=1, weighting=true)
# plotLevel(fineSol, validFine, levLabel="WLS, 7pt", markScale=0.5)

### Weighted LS, with clipping and redistribution
validFine = fineInterpolation!(coarSol, fineSol, order; interpType=3, stencilExtra=0, weighting=false)
fCells = [cellLoc(i, fineSol.dx) for i in validFine]
plot!(fCells, fineSol.data[validFine],
      label="LS, 5pt", markersize = 4, markershape = :circle, markeropacity = 0.7,)
(minBnd, maxBnd) = clipRedist!(coarSol, fineSol, validFine; clipType=2, stencilRadius=2, conservative=false)
plot!(fCells, fineSol.data[validFine],
      label="C&R LS, 5pt", markersize = 4, markershape = :rect, markeropacity = 0.3, linestyle = :dash)
plot!(fCells, minBnd.data[validFine], label="min bound")
plot!(fCells, maxBnd.data[validFine], label="max bound")
medianFilter!(view(fineSol.data, validFine), 1)
plot!(fCells, fineSol.data[validFine],
      label="Smoothed", markersize = 4, markershape = :star, markeropacity = 0.3, linestyle = :dot)

# validFine = fineInterpolation!(coarSol, fineSol, order; interpType=2, stencilExtra=1, weighting=true)
# plotLevel(fineSol, validFine, levLabel="WLS, 7pt", markScale=0.5)
# (minBnd, maxBnd) = clipRedist!(coarSol, fineSol, validFine; stencilRadius=2, conservative=false)
# plotLevel(minBnd, validFine, levLabel="min bound", markScale=0.2)
# plotLevel(maxBnd, validFine, levLabel="max bound", markScale=0.2)
# plotLevel(fineSol, validFine, levLabel="C&R WLS, 7pt", markScale=0.5)


### Conservative LS
# validFine = fineInterpolation!(coarSol, fineSol, order; interpType=3, stencilExtra=0, weighting=false)
# plotLevel(fineSol, validFine, levLabel="Consv, 5pt", markScale=0.5)

# validFine = fineInterpolation!(coarSol, fineSol, order; interpType=3, stencilExtra=1, weighting=false)
# plotLevel(fineSol, validFine, levLabel="Consv, 7pt", markScale=0.5)


### Conservative, Weighted LS
# validFine = fineInterpolation!(coarSol, fineSol, order; interpType=3, stencilExtra=0, weighting=true)
# plotLevel(fineSol, validFine, levLabel="Consv WLS, 5pt", markScale=0.5)

# validFine = fineInterpolation!(coarSol, fineSol, order; interpType=3, stencilExtra=1, weighting=true)
# plotLevel(fineSol, validFine, levLabel="Consv WLS, 7pt", markScale=0.5)

gui()

### Paper results. 5pt LS with high-order clipping and redistribution
for case in [0, 2, 3, 4, 5, 6]
    setInitialData(coarSol, case)
    open("coarData"*string(case)*".dat", "w") do io
        cellPts = [cellLoc(i, coarSol.dx) for i in eachindex(coarSol.data)]
        writedlm(io, hcat(cellPts, coarSol.data))
    end
    validFineI = fineInterpolation!(coarSol, fineSol, order; interpType=3, stencilExtra=0, weighting=false)
    fCellsI = [cellLoc(i, fineSol.dx) for i in validFineI]
    writeDataI = hcat(fCellsI, fineSol.data[validFineI])
    (minBndI, maxBndI) = clipRedist!(coarSol, fineSol, validFineI; clipType=2, stencilRadius=2, conservative=false)
    open("fineData"*string(case)*".dat", "w") do io
        writedlm(io, hcat(writeDataI, fineSol.data[validFineI], minBndI.data[validFineI], maxBndI.data[validFineI]))
    end
end
