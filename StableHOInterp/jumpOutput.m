clear;

% Coarse Level (10 cells spanning jump)
rho_c = [
1.59866322e-01 1.59866322e-01 1.59866322e-01 1.59866067e-01 1.59868493e-01 1.59909954e-01 2.00345910e-01 1.08903533e+00 1.13439260e+00 1.13490482e+00 1.13489994e+00 1.13490000e+00 1.13490000e+00 1.13490000e+00];
energy_c = [
2.64862351e+05 2.64861750e+05 2.64867325e+05 2.64957231e+05 2.42042978e+05 -2.56975330e+05 -2.82389187e+05 -2.82681993e+05 -2.82681665e+05 -2.82681675e+05 -2.82681675e+05];

% Fine Level, refined by 2 (6 coarse cells spaning jump)
rho_f2 = [
1.60809696e-01 1.58927301e-01 1.74730308e-01 1.45089614e-01 6.34929092e-02 3.37198936e-01 9.51347496e-01 1.22672316e+00 1.14841239e+00 1.12037281e+00 1.13589255e+00 1.13391710e+00 ];
energy_f2 = [
2.64316100e+05 2.65418580e+05 2.56649623e+05 2.73264875e+05 3.18923856e+05 1.65162182e+05 -1.79673838e+05 -3.34276819e+05 -2.90269089e+05 -2.74509285e+05 -2.83234567e+05 -2.82130132e+05];

% Fine Level, refined by 4 (6 coarse cells spaning jump)
rho_f4 = [
1.61482064e-01 1.60080859e-01 1.59060771e-01 1.58737363e-01 1.87142949e-01 1.61242733e-01 1.44765717e-01 1.44338576e-01 7.55969087e-03 1.22926750e-01 2.61505792e-01 4.16392703e-01 8.71843479e-01 1.02736402e+00 1.16679618e+00 1.28316264e+00 1.14870878e+00 1.14917225e+00 1.13350633e+00 1.10829552e+00 1.13606570e+00 1.13578074e+00 1.13470542e+00 1.13319013e+00];
energy_f4 = [
2.63895243e+05 2.64704510e+05 2.65300692e+05 2.65504021e+05 2.49080109e+05 2.63616880e+05 2.72854561e+05 2.73072932e+05 3.52315817e+05 2.87496606e+05 2.09645831e+05 1.22643245e+05 -1.36993255e+05 -2.24312674e+05 -3.02593120e+05 -3.67918769e+05 -2.89846676e+05 -2.90098148e+05 -2.81293809e+05 -2.67131405e+05 -2.83296368e+05 -2.83138011e+05 -2.82536728e+05 -2.81688779e+05];

x_c = [-1.5:12];
x_f2 = 2 + [.5:12]/2;
x_f4 = 2 + [.5:24]/4;

figure(1);
clf;
plot(x_c,rho_c,'s-')
labels = ["coarse - Nate"];
grid on
hold on
plot(x_f2,rho_f2,'s-')
plot(x_f4,rho_f4,'s-')
labels = [labels, "fine 2 - Nate", "fine 4 - Nate"];

% Build up the LS fit matrix
xc = [-3.5:3.5]'; % face locations
Ni = 7;
Np = 4;
x0 = 0;
x = xc;
dx = x - x0;
Ac = zeros(Ni,Np);
for p=1:Np
    Ac(:,p) = ((dx(2:Ni+1).^p - dx(1:Ni).^p) ./ ...
        (x(2:Ni+1) - x(1:Ni))) / (p);
end

% Build up the rhs for the 4 fine cells
xf = [-2:2]'/4;  % face locations on fine grid
x = xf;
Ni = 4;
dx = x - x0;
Af = zeros(Ni,Np);
for p=1:Np
    Af(:,p) = ((dx(2:Ni+1).^p - dx(1:Ni).^p) ./ ...
        (x(2:Ni+1) - x(1:Ni))) / (p);
end

% s5 is the stencil for the 4 fine values from the 5 coarse values
s5 = pinv(Ac(2:6,:)')*Af'; % 5pt stencil
w = (max(1,abs(Ac(1:7,2)))).^(-2);
W = diag(w);
% s7 is the WLS stencil for the 4 fine values from the 7 coarse values
s7 = W*pinv(Ac(1:7,:)'*W)*Af'; % 7pt stencil

% Plot the values for 5pt LS, 7pt WLS fit
xf_i = [];
rho5lsf = [];
rho7wlsf = [];
for ic=4:11
    xc0 = ic-.5;
    xf_i = [xf_i; Af(:,2) + x_c(ic)];
    rho5lsf = [rho5lsf; s5'*rho_c((-2:2)+ic)'];
    rho7wlsf = [rho7wlsf; s7'*rho_c((-3:3)+ic)'];
end
plot(xf_i, rho5lsf, 'gs-');
plot(xf_i, rho7wlsf, 'ms-');
labels = [labels, "5 pt ls", "7 pt wls"];

legend(labels);

figure(2);
clf;

plot(x_c,rho_c,'s-')
labels = ["coarse - Nate"];
grid on
hold on
% plot(x_f2,rho_f2,'s-')
% labels = [labels, "fine 2 - Nate"];
plot(x_f4,rho_f4,'s-')
labels = [labels, "fine 4 - Nate"];

minc = [];
maxc = [];
dm = [];
for ic=4:11
    xc0 = ic-.5;
    ixc = (-2:2)+ic;
    minc = [minc; min(rho_c(ixc))];
    maxc = [maxc; max(rho_c(ixc))];
    ixf = 4*(ic-3)+[-3:0];
    dm = sum(rho5lsf(ixf) - rho_c(ic))/4;
    plot(xf_i(ixf), rho5lsf(ixf) - dm*ones(4,1), 'ms-');
%     plot(xf_i(ixf), rho7wlsf(ixf), 'ks-');
end

labels = [labels, "5 pt ls - dM shift"]; % , "7 pt wls - redist"];
legend(labels);


