# == Include other code ==
push!(LOAD_PATH, pwd()*"/../LeastSquares")
include("../LeastSquares/LinearLS.jl")
using SparseArrays
using DelimitedFiles
using BasisFunctions
const BF = BasisFunctions

# == Run parameters ==
dim = 1
length = 1.
nCells = 16
refRatio = 4

Float = Float64

# == Setup data structures ==
struct DataLevel
    data::Array
    dx::Float
    refRatio::Int
end

import Base.getindex
# acts like an array
function Base.getindex(d::DataLevel, i...)
    return getindex(d.data, i...)
end

function Base.lastindex(d::DataLevel, i...)
    return lastindex(d.data, i...)
end

function Base.setindex!(d::DataLevel, i...)
    return setindex!(d.data, i...)
end

function makeFinerLevel(coarseLevel::DataLevel,
                        refRatio::Int)::DataLevel
    return DataLevel(
        typeof(coarseLevel.data)(undef, size(coarseLevel.data).*refRatio),
        coarseLevel.dx/refRatio,
        refRatio)
end

coarU = DataLevel(Array{Float, dim}(undef, nCells),
                    length/nCells, 1)
fineU = makeFinerLevel(coarU, refRatio)

coarJU = DataLevel(Array{Float, dim}(undef, nCells),
                    length/nCells, 1)
fineJU = makeFinerLevel(coarU, refRatio)

coarJ = DataLevel(Array{Float, dim}(undef, nCells),
                    length/nCells, 1)
fineJ = makeFinerLevel(coarJ, refRatio)

coardJ = DataLevel(Array{Float, dim}(undef, nCells),
                    length/nCells, 1)
finedJ = makeFinerLevel(coardJ, refRatio)

coardU = DataLevel(Array{Float, dim}(undef, nCells),
                    length/nCells, 1)
finedU = makeFinerLevel(coardU, refRatio)

# The mapping function from xi to x
# Should preserve domain length
# ## Tangent
# sf = 1.5 # st reching factor
# a = sf*(pi/4)*2
# b = length/2
# c = (2*tan(sf*(pi/4)))
# mapping(xi) = tan.(a.*(xi .- b))./c .+ b
# mappingD(xi) = (sec.(a*(xi - b)).^2)*a/c
# mappingDD(xi) = (tan.(a*(xi - b)).^2).*(sec.(a*(xi - b)).^2)*(a^2)/c
# mappingI(xi) = -log.(cos.(a*(xi - b)))/(a*c) + xi*b
# mapName="Tan"

## Linear
# mapping(xi) = xi
# mappingD(xi) = 1
# mappingDD(xi) = 0
# mappingI(xi) = 0.5*xi.^2

## Poly
pd = 2
off = 0
coef = 1
mapping(xi) = coef*(xi.+off).^pd
mappingD(xi) = coef*pd*(xi.+off).^(pd-1)
mappingDD(xi) = coef*pd*(pd-1)*(xi.+off).^(pd-2)
mappingI(xi) = coef*((xi.+off).^(pd+1))/(pd+1)
mapName = "Poly"

mapBounds = [mapping(0), mapping(1)]
mapLength = (mapBounds[2] - mapBounds[1])

function cellLoc(index, dx, node=false)
    offset = if !node 0.5 else 0 end
    return (index .- 1 .+ offset).*dx
end

function mappedLoc(index, dx, node=false)
    return mapping(cellLoc(index, dx, node))
end

function fineToCoarseIdx(fineIndex, refRatio)
    return div((fineIndex.-1),refRatio) .+ 1
end

function coarseToFineIdxs(coarseIndex, refRatio)
    startFineIdx = (coarseIndex.-1).*refRatio .+ 1
    return startFineIdx:startFineIdx.+refRatio.-1
end

function growInterval(intv, grow)
    return intv.start-grow:intv.stop+grow
end

function getValidCoarse(validFine, refRatio)
    return Int((validFine.start-1)/refRatio+1):Int((validFine.stop)/refRatio)
end

# Analytic <J>
function setJ!(J)
    for i in eachindex(J.data)
        J[i] = (mappedLoc(i+1, J.dx, true)
                - mappedLoc(i, J.dx, true))/(J.dx)
    end
end

# 2nd order d/dx <J> = d/dx J
function setdJ!(dJ)
    for i in eachindex(dJ.data)
        dJ[i] = mappingDD(cellLoc(i, dJ.dx, false))
    end
end

# 2nd order d/dx <U>, at 2nd order <U> = U
function solvedU2!(dU, U, interval=1:size(U.data,1))
    for i in growInterval(interval, -1)
        dU[i] = (U[i+1] - U[i-1])/(2*U.dx)
    end
    i = interval.start
    dU[i] = (-3*U[i] + 4*U[i+1] - U[i+2])/(2*U.dx)
    i = interval.stop
    dU[i] = -(-3*U[i] + 4*U[i-1] - U[i-2])/(2*U.dx)
end

function applyEach!(func, range, inputs...)
    for i in range
        func(inputs...)
    end
end

function JUfromU2(J, U, e...)
    return J .* U
end

function JUfromU4(J, U, dJ, dU, dx)
    return JUfromU2(J, U) .+ ((dx^2)/12) * dU .* dJ
end

function UfromJU2(JU, J, e...)
    return JU ./ J
end

function UfromJU4(JU, J, dJ, dU, dx)
    return UfromJU2(JU, J) .- (((dx^2)/12) * dU .* dJ)./J
end

function JUfromU2!(JU, J, U)
    for i in eachindex(JU.data)
        JU[i] = U[i] * J[i]
    end
end

function JUfromU4!(JU, J, U, dJ, dU)
    JUfromU2!(JU, J, U)
    for i in eachindex(JU.data)
        JU[i] += ((U.dx^2)/12) * dU[i] * dJ[i]
    end
end

function UfromJU2!(U, JU, J)
    for i in eachindex(U.data)
        U[i] = JU[i] / J[i]
    end
end

function UfromJU4!(U, JU, J, dJ, dU)
    UfromJU2!(U, JU, J)
    for i in eachindex(JU.data)
        U[i] -= (((U.dx^2)/12) * dU[i] * dJ[i])/J[i]
    end
end

# == define coarse data ==
function setInitialData(solution, initState)
    if initState == 0
        # Use a step condition
        stepLoc = (mapBounds[2] + mapBounds[1])/2
        leftState = 1.0
        rightState = 0.0 #-1.0
        for i in eachindex(solution.data)
            if mappedLoc(i, solution.dx)[] < stepLoc
                solution[i] = leftState
            else
                solution[i] = rightState
            end
        end
    elseif initState == 1
        # Use a sin wave
        amp = 1.0
        period = mapLength
        for i in eachindex(solution.data)
            solution[i] = amp*sin(mappedLoc(i, solution.dx)*(2*pi/period))
        end
    elseif initState == 2
        # Saw tooth
        amp = 2.0
        period = mapLength
        offset = 0 #-1.0
        for i in eachindex(solution.data)
            solution[i] = amp*mod(mappedLoc(i, solution.dx),(period/2)) + offset
        end
    elseif initState == 3
        # Smoothed step
        stepLoc = (mapBounds[2] + mapBounds[1])/2
        amp = 0.5
        offset = 0.5
        period = 0.2
        for i in eachindex(solution.data)
            solution[i] = -amp*tanh((mappedLoc(i, solution.dx) - stepLoc)*(2*pi/period)) + offset
        end
    elseif initState == 4
        # Gaussian
        cent = (mapBounds[2] + mapBounds[1])/2
        amplitude = 1.
        stdDev = 0.1
        for i in eachindex(solution.data)
            solution[i] = amplitude.*exp(-(mappedLoc(i, solution.dx)-cent)^2/(2*stdDev^2))
        end
    elseif initState == 5
        # Discontinuous piecewise polynomial
        amp = 0.5 #1.0
        period = (mapBounds[2] + mapBounds[1])/2
        offset = 0.5 #0
        order = 3
        unitLen = 2
        pp = BF.LinearFunction(BF.MonomialBasis(order))
        pp.coef[:] = zeros(order+1)
        pp.coef[end] = 1
        for i in eachindex(solution.data)
            # point valued
            # x = mod(cellLoc(i, solution.dx), period)*(unitLen/period) - unitLen/2
            # solution[i] = amp*BF.eval(pp, x)
            # average valued
            x = mod.(mappedLoc([i, i+1], solution.dx, true),
                     period+eps()).*(unitLen/period) .- unitLen/2
            solution[i] = amp * BF.integral(pp,
                                            x[1],
                                            x[2])/(solution.dx*(unitLen/period)) .+ offset
        end
    elseif initState == 6
        # Point
        for i in eachindex(solution.data)
            solution.data[i] = 0
        end
        # solution.data[round(Int, nCells/2)] = 1
        solution.data[11] = 1
    end
end

# == Conservation maintenance ==
function conservationCalc(coarseCell, fineCells, report=false)
    consvSum = coarseCell - sum(fi for fi in fineCells)/refRatio
    if report
        println("Cell conservation error = ", consvSum)
    end
    return consvSum
end

function conservationCheck(coarse, fine, validFine, report=false)
    consvCheck = 0
    maxValMag = 0
    conservative = true
    for ci in getValidCoarse(validFine, refRatio)
        ciCons = conservationCalc(coarse[ci],
                                  fine[coarseToFineIdxs(ci, refRatio)],
                                  report)
        maxValMag = maximum(abs.(fine[coarseToFineIdxs(ci, refRatio)]))
        isciCons = abs(ciCons) < maxValMag*eps()
        conservative = conservative && isciCons
        consvCheck = max(consvCheck, abs(ciCons))
    end
    if report
        println("Solution conservation is ", conservative)
        println("  max conservation error = ", consvCheck)
    end
    return conservative
end

function conservationCorrect!(coarse, fine, validFine)
    for ci in validCoarse
        consVal = coarse[ci]
        for fi in coarseToFineIdxs(ci, refRatio)
            consVal -= fine[fi]./refRatio
        end
        if consVal != 0
            for fi in coarseToFineIdxs(ci, refRatio)
                fine[fi] += consVal
            end
        end
    end
end

# == interpolate the fine level ==
function fineInterpolation!(coarJU, fineJU, coarJ, fineJ, coarU, fineU, coardJ, finedJ, N;
                            interpType=3, stencilExtra=0, weighting=true)
    npolyBaseStenSize = cld(N,2) # stencil extent in each direction not including center
    stencilSize = npolyBaseStenSize + stencilExtra
    validFine = stencilSize*refRatio+1:(nCells-stencilSize)*refRatio
    if interpType == 3
        # Nth order conservative cell average interpolation
        # Do a polynomial (possibly least squares) fit in each coarse cell, eval for each fine contained
        # See eq 50 of McCorquodale 2011
        normStencil = -stencilSize:stencilSize
        normStencilNodes = (-stencilSize:stencilSize+1) .- 0.5
        polyFit = BF.LinearFunction(BF.MonomialBasis(N-1))
        # moments divided by cell volume
        M = (makeLinearMomentLS(polyFit, normStencilNodes))./(1)
        W = I
        if weighting
            # Weight to furthest cell extent (Nate weighting, works poorly)
            # W = makeDiagWeights(x -> (1/(abs(x)+0.5)).^(N), normStencil)
            # Weight to cell center (Hans weighting)
            W = makeDiagWeights(x -> min(1, (1/(abs(x))).^(N/2)), normStencil)
        end
        println("Order = ", N, "\tStencil size = ", size(normStencil))
        consvCheck = 0;
        for ci in 1+stencilSize:nCells-stencilSize
            # Add conservation constraint <JU> = prod(J,M)
            # This is the constraint on the coarse level, which is wrong for conservation
            # B = reshape([JUfromU4(coarJ[ci], M[stencilSize+1, i], coardJ[ci], coardU[ci], coarJ.dx) for i in 1:N], 1, :)
            # Instead we need to constrain the sum of the fine level equals the existing coarse
            # It is a subtle difference but actually enforces conservation
            B = reshape([JUfromU4(coarJ[ci], M[stencilSize+1, i], coardJ[ci], coardU[ci], coarJ.dx) for i in 1:N], 1, :)
            # Solve the polynomial
            y = view(coarU.data, normStencil.+ci)
            d = [coarJU[ci]]
            polyFit.coef[:] = linearConstrainedLS(M, y, B, d)
            xi0 = ci + 0.5
            JUcons = coarJU[ci] - sum([JUfromU4(coarJ[ci], bfI, coardJ[ci], coardU[ci], coarJ.dx)
                       for bfI in BF.integralFuncs(polyFit,
                                                   -0.5,
                                                   0.5)])
            # interpolate fine cells
            consvSum = coarJU[ci]
            for fi in coarseToFineIdxs(ci, refRatio)
                xifCell = ([fi, fi+1] .-1)./refRatio .+ 1 .- xi0
                xifcenter = (xifCell[2] + xifCell[1])/2
                fineU[fi] = BF.integral(polyFit,
                                        xifCell[1],
                                        xifCell[2]).*(refRatio)
                finedU[fi] = BF.derivative(polyFit, xifcenter, 1)
                fineJU[fi] = sum([JUfromU2(fineJ[fi], bfI, finedJ[fi], finedU[fi], fineJ.dx)
                                  for bfI in BF.integralFuncs(polyFit,
                                                              xifCell[1],
                                                              xifCell[2])]).*(refRatio)

                consvSum -= fineJU[fi]./refRatio
            end
            # HACK::Not conservative, due to 4th order product error so just adjust it
            for fi in coarseToFineIdxs(ci, refRatio)
                fineJU[fi] += consvSum #/refRatio
                # dU should not change, only adding a constant
                fineU[fi] = UfromJU4(fineJU[fi], fineJ[fi], finedJ[fi], finedU[fi], fineJ.dx)
            end
            consvCheck = max(consvCheck, abs(consvSum))
        end
        println("max conservation error = ", consvCheck)
    end
    return validFine
end

# == Filters to use ==
function medianFilter!(data, iter=1)
    sz = prod(size(data))
    F = spdiagm(1 => ones(sz-1)/4, -1 => ones(sz-1)/4, 0 => 2*ones(sz)/4)
    F[1,1] += 1/4
    F[sz,sz] += 1/4
    for i in 1:iter
        data[:] = F*data
    end
    ## Same thing if you wanted to hold onto the matrix
    # F = F^iter
    # data[:] = F*data
end

# == Clipping and Redistribution ==
function clipRedist!(coarU, fineJU, fineU, fineJ, finedJ, finedU, validFine; redistType=1, clipType=1, iter=1, stencilRadius=1)
    # Need to use 2nd order U
    UfromJU2!(fineU, fineJU, fineJ)
    # Get valid coarse
    validCoarse = getValidCoarse(validFine, refRatio)
    # make bounds for the clipping (β)
    minBnd = zeros(size(fineU.data))
    maxBnd = zeros(size(fineU.data))
    enableClipping = fill(true, size(fineU.data))
    if clipType >= 1 # 1st order bounds
        normStencil = -stencilRadius:stencilRadius
        for ci in 1+stencilRadius:nCells-stencilRadius
            stenMin = minimum(coarJU[normStencil.+ci]./coarJ[normStencil.+ci])
            stenMax = maximum(coarJU[normStencil.+ci]./coarJ[normStencil.+ci])
            # stenMin = minimum(coarU[normStencil.+ci])
            # stenMax = maximum(coarU[normStencil.+ci])
            for fi in coarseToFineIdxs(ci, refRatio)
                minBnd[fi] = stenMin
                maxBnd[fi] = stenMax
            end
        end
    end
    if clipType == 2 # High order clipping
        # compute 2nd derivative of each coarse cell
        # Order doesn't matter, just want the sign
        secondDeriv = deepcopy(coarU)
        normStencil = -stencilRadius:stencilRadius
        for ci in 1+stencilRadius:nCells-stencilRadius
            secondDeriv[ci] = (coarU[ci-1] - 2*coarU[ci] + coarU[ci+1])/(2*coarU.dx^2)
        end
        # @show secondDeriv
        for ci in 1+stencilRadius:nCells-stencilRadius
            smooth = (sign(secondDeriv[ci-1]) ==
                      sign(secondDeriv[ci]) ==
                      sign(secondDeriv[ci+1]))
            for fi in coarseToFineIdxs(ci, refRatio)
                enableClipping[fi] = !smooth
            end
        end
    end
    # smooth the bounds
    medianFilter!(view(minBnd, validFine), 4)
    medianFilter!(view(maxBnd, validFine), 4)

    # Track the regions that need clipped (δU)
    extraMass = zeros(size(fineU.data))
    for ci in validCoarse
        for fi in coarseToFineIdxs(ci, refRatio)
            extraMass[fi] = max(fineU[fi]-maxBnd[fi], 0)
            if extraMass[fi] == 0
                extraMass[fi] = min(fineU[fi]-minBnd[fi], 0)
            end
        end
    end
    @show extraMass[validFine]
    # simple clipping, local to each coarse cell
    # Maintaints a conservative solution, but does not check for it
    if redistType == 1
        for ci in validCoarse
            fineCells = coarseToFineIdxs(ci, refRatio)
            toRedist = sum(JUfromU2(extraMass[fineCells], fineJ[fineCells]))
            # clip JU -= δJU and U -= δU
            if all(enableClipping[fineCells]) && (toRedist != 0)
                println("== ", ci, " ==")
                @show fineJU[fineCells]
                @show fineU[fineCells]
                @show fineJ[fineCells]
                @show sum(fineJU[fineCells])
                @show coarJU[ci]
                @show coarJ[ci]
                @show coarJU[ci]/coarJ[ci]
                @show sum(fineU[fineCells])
                @assert fineJU[fineCells] ≈ fineJ[fineCells].*fineU[fineCells]
                for fi in fineCells
                    fineU[fi] -= extraMass[fi]
                    fineJU[fi] -= JUfromU2(extraMass[fi], fineJ[fi])
                end
                @show toRedist
                # redistribute
                allowance = zeros(size(fineCells)) #α for all of R
                if toRedist > 0
                    allowance = max.((maxBnd[fineCells] - fineU[fineCells]), 0)
                elseif toRedist < 0
                    allowance = min.((minBnd[fineCells] - fineU[fineCells]), 0)
                end
                #allowance = allowance .* fineJ[fineCells]
                @show allowance
                @show sum(allowance)
                # doesn't quite apply for mapped, since we don't care about the space available in JU, only the conservation
                #@assert abs(sum(allowance) - toRedist) < sqrt(eps())
                if (sum(allowance) != 0)
                    @warn sum(abs.(allowance)) >= sum(abs.(toRedist)) - sqrt(eps(maximum(abs.(allowance))))
                    @assert sum(toRedist.*(allowance./sum(allowance))) ≈ toRedist
                    fineJU[fineCells] += toRedist.*(allowance./sum(allowance))
                end
                @show fineJU[fineCells]
                @show fineJU[fineCells]./fineJ[fineCells]
                @show sum(fineJU[fineCells])
                @show sum(fineJU[fineCells]./fineJ[fineCells])/refRatio
                @show sum(fineU[fineCells])
            end
        end
    end
    @show fineJU[validFine]
    # update the fine U
    finedU = deepcopy(fineU)
    # UfromJU2!(fineU, fineJU, fineJ)
    # @show fineU
    UfromJU2!(fineU, fineJU, fineJ)
    # solvedU2!(finedU, fineU, validFine)
    # UfromJU4!(fineU, fineJU, fineJ, finedJ, finedU)
    # return min/max
    return (minBnd, maxBnd)
end

# == Plotting ==
using Plots
#pyplot()
gr()
plot(xlim = (mapping(0),mapping(1)))
#plot(ylim = (-1.5,1.5))
#plot(ylim = (-0.5,1.5))

function plotLevelGrid(level, plotInterval=eachindex(level.data);
                       markScale=1/level.refRatio,
                       levLabel="")
    # plot the grid lines
    nodes = [mappedLoc(i, level.dx, true) for i in plotInterval]
    y = 0*nodes
    plot!(nodes, y,
          label = :none,#levLabel*" grid",
          linecolor = :black,
          markercolor = :black,
          markersize = 20*markScale,
          markershape = :vline,
          primary = false)
end

function plotLevel(level, plotInterval=eachindex(level.data);
                   markScale=1/level.refRatio,
                   levLabel="")
    # plot the cell centers
    cells = [mappedLoc(i, level.dx) for i in plotInterval]
    plot!(cells, level[plotInterval],
          label = levLabel*" solution",
          markersize = 8*markScale,
          markershape = :circle)
end

### The "main" program, where everything get called from
# initialized the coarse solution
order = 4
setInitialData(coarU, 5)

# setup J
setJ!(coarJ)
setJ!(fineJ)

println("-- Conservative J = ",
        conservationCheck(coarJ, fineJ, 1:size(fineJ.data,1), false))

# setup JU from U and J
setdJ!(coardJ)
setdJ!(finedJ)

solvedU2!(coardU, coarU)
#JUfromU4!(coarJU, coarJ, coarU, coardJ, coardU)
JUfromU2!(coarJU, coarJ, coarU)

# plot the grids
plotLevelGrid(coarU, levLabel="coarse")
validFine = 1:refRatio*nCells
plotLevelGrid(fineU, validFine, levLabel="fine")
plotLevel(coarU, levLabel="coarse")

# write the grids
open("coar"*mapName*"Grid.dat", "w") do io
    gridPts = [mappedLoc(i, coarU.dx, true) for i in 1:nCells+1]
    writedlm(io, gridPts)
end
open("fine"*mapName*"Grid.dat", "w") do io
    gridPts = [mappedLoc(i, fineU.dx, true) for i in 1:refRatio*nCells+1]
    writedlm(io, gridPts)
end

### LS
# validFine = fineInterpolation!(coarU, fineU, order; interpType=2, stencilExtra=0, weighting=false)
# plotLevel(fineU, validFine, levLabel="LS, 5pt", markScale=0.5)

# validFine = fineInterpolation!(coarU, fineU, order; interpType=2, stencilExtra=1, weighting=false)
# plotLevel(fineU, validFine, levLabel="LS, 7pt", markScale=0.5)


### Weighted LS
# validFine = fineInterpolation!(coarU, fineU, order; interpType=2, stencilExtra=0, weighting=true)
# plotLevel(fineU, validFine, levLabel="WLS, 5pt", markScale=0.5)

# validFine = fineInterpolation!(coarU, fineU, order; interpType=2, stencilExtra=1, weighting=true)
# plotLevel(fineU, validFine, levLabel="WLS, 7pt", markScale=0.5)

### Weighted LS, with clipping and redistribution
validFine = fineInterpolation!(coarJU, fineJU, coarJ, fineJ, coarU, fineU, coardJ, finedJ, order; interpType=3, stencilExtra=0, weighting=false)
println("-- Conservative JU from interp = ",
        conservationCheck(coarJU, fineJU, validFine, true))
@show coarJU
fCells = [mappedLoc(i, fineU.dx) for i in validFine]
plot!(fCells, fineU[validFine],
      label="WLS, 5pt", markersize = 4, markershape = :circle, markeropacity = 0.7)
(minBnd, maxBnd) = clipRedist!(coarU, fineJU, fineU, fineJ, finedJ, finedU, validFine; clipType=1, stencilRadius=2)
println("-- Conservative JU from clipping = ",
        conservationCheck(coarJU, fineJU, validFine, true))
#UfromJU2!(fineU, fineJU, fineJ)
@show fineU[validFine]
plot!(fCells, fineU[validFine],
      label="C&R WLS, 5pt", markersize = 4, markershape = :rect, markeropacity = 0.3, linestyle = :dash)
plot!(fCells, minBnd[validFine], label="min bound")
plot!(fCells, maxBnd[validFine], label="max bound")
# medianFilter!(view(fineU.data, validFine), 1)
# plot!(fCells, fineU[validFine],
#       label="Smoothed", markersize = 4, markershape = :star, markeropacity = 0.3, linestyle = :dot)

coarU2 = deepcopy(coarU)
UfromJU2!(coarU2, coarJU, coarJ)
plotLevel(coarU2, levLabel="coarse 2nd order")



# validFine = fineInterpolation!(coarU, fineU, order; interpType=2, stencilExtra=1, weighting=true)
# plotLevel(fineU, validFine, levLabel="WLS, 7pt", markScale=0.5)
# (minBnd, maxBnd) = clipRedist!(coarU, fineU, validFine; stencilRadius=2, conservative=false)
# plotLevel(minBnd, validFine, levLabel="min bound", markScale=0.2)
# plotLevel(maxBnd, validFine, levLabel="max bound", markScale=0.2)
# plotLevel(fineU, validFine, levLabel="C&R WLS, 7pt", markScale=0.5)


### Conservative LS
# validFine = fineInterpolation!(coarU, fineU, order; interpType=3, stencilExtra=0, weighting=false)
# plotLevel(fineU, validFine, levLabel="Consv, 5pt", markScale=0.5)

# validFine = fineInterpolation!(coarU, fineU, order; interpType=3, stencilExtra=1, weighting=false)
# plotLevel(fineU, validFine, levLabel="Consv, 7pt", markScale=0.5)


### Conservative, Weighted LS
# validFine = fineInterpolation!(coarU, fineU, order; interpType=3, stencilExtra=0, weighting=true)
# plotLevel(fineU, validFine, levLabel="Consv WLS, 5pt", markScale=0.5)

# validFine = fineInterpolation!(coarU, fineU, order; interpType=3, stencilExtra=1, weighting=true)
# plotLevel(fineU, validFine, levLabel="Consv WLS, 7pt", markScale=0.5)

# println("-- Final Conservative J = ",
#         conservationCheck(coarJ, fineJ, validFine, false))
# println("-- Final Conservative JU = ",
#         conservationCheck(coarJU, fineJU, validFine, false))

gui()

### Paper results. 5pt LS with high-order clipping and redistribution
for case in [0, 2, 3, 4, 5, 6]
    setInitialData(coarU, case)
    solvedU2!(coardU, coarU)
    #JUfromU4!(coarJU, coarJ, coarU, coardJ, coardU)
    JUfromU2!(coarJU, coarJ, coarU)
    open("coarData"*mapName*string(case)*".dat", "w") do io
        # cellPts = [cellLoc(i, coarU.dx) for i in eachindex(coarU.data)]
        cellPts =  [mappedLoc(i, coarU.dx, false) for i in eachindex(coarU.data)]
        writedlm(io, hcat(cellPts, coarU.data))
    end
    validFineI = fineInterpolation!(coarJU, fineJU, coarJ, fineJ, coarU, fineU, coardJ, finedJ, order; interpType=3, stencilExtra=0, weighting=false)
    # fCellsI = [cellLoc(i, fineU.dx) for i in validFineI]
    fCellsI = [mappedLoc(i, fineU.dx, false) for i in validFineI]
    writeDataI = hcat(fCellsI, fineU.data[validFineI])
    (minBndI, maxBndI) = clipRedist!(coarU, fineJU, fineU, fineJ, finedJ, finedU, validFineI; clipType=2, stencilRadius=2)
    open("fineData"*mapName*string(case)*".dat", "w") do io
        writedlm(io, hcat(writeDataI, fineU.data[validFineI], minBndI[validFineI], maxBndI[validFineI]))
    end
end
