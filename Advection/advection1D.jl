# == Include other code ==
push!(LOAD_PATH, pwd()*"/../LeastSquares")
include("../LeastSquares/LinearLS.jl")
push!(LOAD_PATH, pwd()*"/../BoxData")
include("../BoxData/LevelData.jl")
include("../BoxData/Stencils.jl")
# using SparseArrays
# using DelimitedFiles
using BasisFunctions
const BF = BasisFunctions
using .LevelDataMod
using .Stencils
using DifferentialEquations
using SparseArrays
using Plots
gr()

# == Run parameters ==
Float = Float64
dim = 1
domLength = 1.
nCells = 64
nGhost = 6

dx = domLength/nCells
cfl = 0.5
dt = cfl*dx
stopTime = 0.5 # profile centered at .75
numIter = ceil(Int, stopTime/dt)
println("dx: ", dx, "\t dt: ", dt, "\t numIter: ", numIter)

# == Setup data structures ==
sol = LevelData(Float, dim, nCells, nGhost)
solIndices = getIndices(sol)
solNodeIndices = getIndices(sol, nodes=true)
xiOrg = collect(first(solNodeIndices).I)
scale = dx
mapping(xi) = scale*(xi.-xiOrg)
solCells = mappedLoc(solIndices, mapping)
solNodes = mappedLoc(solNodeIndices, mapping, true)

# == set the exact profile ==
function setExact(sol; state=0)
    for i in allIndices(sol)
        if state==0
            sol[i] = 1
        elseif state==1
            # Gaussian
            cent = 0.25
            amplitude = 1.
            stdDev = 0.04
            sol[i] = amplitude.*exp.(-(mappedLoc([i], mapping)[1].-cent).^2 ./ (2*stdDev^2))
            if sol[i] < eps(1.)
                sol[i] = 0
            end
        else
            @assert false
        end
    end
end

function makeAdvectionStencil(dx, dim; spaceType=0)::Stencil
    sten = Stencil(0)
    p1 = CartesianIndex(1)
    # define the fluxes (on the low side)
    if spaceType == 0 # second order centered
        sten = Stencil(2)
        sten.cells[:] = [-1*p1, 0*p1]
        sten.weights[:] = [1, 1]./(2*dx)
    elseif spaceType == 1 # fourth order centered
        sten = Stencil(4)
        sten.cells[:] = [-2*p1, -1*p1, 0*p1, 1*p1]
        sten.weights[:] = [-1, 7, 7, -1]./(12*dx)
    elseif spaceType == 2 # first order upwind (left)
        sten = Stencil(1)
        sten.cells[:] = [-1*p1]
        sten.weights[:] = [1]./(1*dx)
    elseif spaceType == 3 # second order upwind (left)
        sten = Stencil(3)
        sten.cells[:] = [-2*p1, -1*p1, 0*p1]
        sten.weights[:] = [-1, 4, 1]./(4*dx)
    elseif spaceType == 4 # third order upwind (left)
        sten = Stencil(3)
        sten.cells[:] = [-2*p1, -1*p1, 0*p1]
        sten.weights[:] = [-1, 5, 2]./(6*dx)
    end
    # combines for a cell update
    stenL = deepcopy(sten)
    stenR = deepcopy(sten)
    shift!(stenR, p1)
    stenR.weights .*= -1
    sten = stenL + stenR
    return sten
end

function applyStencilOp!(opval, sol, params, t)
    stencil, region = params
    applyStencilForEach!(opval, stencil, [sol], region)
end

function advectAdv!(sol, sten, numIter, dt; timeType=0)
    interior = getIndices(sol)
    opParams = [sten, interior]
    odeForm = ODEProblem(applyStencilOp!, sol[:], (0., dt*numIter), opParams)
    if timeType==-1
        opval = deepcopy(sol)
        for i = 1:numIter
            op(opval[:], sol[:], opParams, spaceType)
            sol[interior] = sol[interior] .+ dt.*opval[interior] # explicit euler
        end
        tpts = dt.*(1:numIter)
    elseif timeType==0
        (sols, tpts) = solve(odeForm, Euler(), dt=dt)
        sol[:] = sols[end]
    elseif timeType==1
        (sols, tpts) = solve(odeForm, Heun(), dt=dt)
        sol[:] = sols[end]
    elseif timeType==2
        (sols, tpts) = solve(odeForm, RK4(), dt=dt)
        sol[:] = sols[end]
    else
        @assert false
    end
    return tpts
end

# Set up
setExact(sol; state=1)
# Plot the initial solution
pltSol = plot()
plot!(solCells, sol[solIndices],
      linewidth=2, label="Init")

# Solve
advectSten = makeAdvectionStencil(dx, dim; spaceType=4)
tpts = advectAdv!(sol, advectSten, numIter, dt; timeType=2)
# Plot the final solution
plot!(solCells, sol[solIndices],
      linewidth=2, label="Final")
#display()

# Make the system as a matrix and show connection
SM = spzeros(nCells, nCells)
for i in 1:length(solIndices)
    SM[i,i] = 1
    for (j, w) in zip(advectSten.cells, advectSten.weights)
        if (j + solIndices[i]) in solIndices
            SM[i, j + CartesianIndex(i)] += w
        end
    end
end
pltMat = spy(SM)
# Show the eigenvalues for this operator
ev = eigvals(Matrix(SM))
pltEigs = scatter(real(ev), imag(ev))

plot(pltSol, pltMat, pltEigs, layout = (3,1), legend = false)
