using DelimitedFiles
using Plots
gr()

# A00 = [0.7 0.3; 0.3 -0.7]
# A10 = [0.7 0.3; 0 1.]
# A01 = [1. 0; 0 1.]

# # cartesian
# A00 = [1. 0;
#        0  1.]
# A10 = [1. 0;
#        0  1.]
# A01 = [1. 0;
#        0  1.]
# map 1
A00 = [.6  .1;
       -.4 .9]
A10 = [.7 -.5;
       .3 .5]
A01 = [.9 0;
       0  1.]

# mapping function
fA(xi, eta) = A00 + xi*(A10 - A00) + eta*(A01 - A00)
offset(xi, eta) = [0, 0];
mapF(xi, eta) = fA(xi, eta)*[xi, eta] + offset(xi, eta)

res = 0.1; #coarse
Xi = 0:res:1
Eta = 0:res:1

# res = 0.1/2; #mid
# Xi = 0:res:1
# Eta = 0:res:1

# res = 0.1/4; #fine
# Xi = 0:res:1
# Eta = 0:res:1

p = plot()

# XY_xi0 = hcat([mapF(xi, 0) for xi in Xi]...)
# plot!(p, XY_xi0[1,:], XY_xi0[2,:], seriestype = :scatter)

# XY_xi1 = hcat([mapF(xi, 1) for xi in Xi]...)
# plot!(p, XY_xi1[1,:], XY_xi1[2,:], seriestype = :scatter)

# XY_0eta = hcat([mapF(0, xi) for xi in Xi]...)
# plot!(p, XY_0eta[1,:], XY_0eta[2,:], seriestype = :scatter)

# XY_1eta = hcat([mapF(1, xi) for xi in Xi]...)
# plot!(p, XY_1eta[1,:], XY_1eta[2,:], seriestype = :scatter)

open("gridDataCrOnlyYX.txt", "w") do io
    #full grid
    for xi in Xi
        XY_xieta = hcat([mapF(xi, eta) for eta in Eta]...)
        writedlm(io, XY_xieta)
        plot!(p, XY_xieta[1,:], XY_xieta[2,:], seriestype = :scatter)
    end

    display(p)
end
